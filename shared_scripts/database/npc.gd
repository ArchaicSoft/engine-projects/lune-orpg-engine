class_name Npc extends IndexedResource

static func _type_directory() -> String:  return Paths.npcs()

# basic descriptors
@export var dialogue      : String                = ""
@export var sound         : String                = ""
@export var sprite        : int                   = -1

# mechanics
@export var type          : Globals.Npc           = Globals.Npc.friendly
@export var animation     : int                   = -1
@export var damage        : int                   = 0
@export var level         : int                   = 1
@export var stats         : Array[int]            = Utility.array_presize(Globals.Stat.keys().size(),  0)
@export var vitals        : Array[int]            = Utility.array_presize(Globals.Vital.keys().size(), 0)

# misc properties
@export var rewards       : Array[ChanceItemSlot]
@export var respawn_time  : int                   = 60
@export var view_distance : int                   = 1
