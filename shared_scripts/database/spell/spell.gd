class_name Spell extends IndexedResource

static func _type_directory() -> String:  return Paths.spells()

# basic descriptors
@export var description      : String             = "A brand new spell is available."
@export var sound            : String             = ""
@export var image            : int                = -1

# mechanics
@export var type             : Globals.Spell      = Globals.Spell.status_mod
@export var animation_cast   : int                = -1
@export var animation_effect : int                = -1
@export var range_cast       : int                = 1
@export var range_effect     : int                = 1
@export var time_cast        : int                = 0
@export var time_cooldown    : int                = 0
@export var time_stun        : int                = 0
@export var requirements     : PlayerRequirements = PlayerRequirements.new()

# modify or buff status
@export var modifies_stats   : Array[int]         = Utility.array_presize(Globals.Stat.keys().size(),  0)
@export var modifies_vitals  : Array[int]         = Utility.array_presize(Globals.Vital.keys().size(), 0)
@export var cast_over_time   : CastOverTime       = CastOverTime.new()

# warp
@export var warp_map         : int                = 0
@export var warp_pos_x       : float              = 0.0
@export var warp_pos_y       : float              = 0.0
@export var warp_facing      : Globals.Direction  = Globals.Direction.down

# public accessors
func get_warp_position()               -> Vector2: return Vector2(warp_pos_x, warp_pos_y)
func set_warp_position(value: Vector2) -> void:    warp_pos_x = value.x; warp_pos_y = value.y
