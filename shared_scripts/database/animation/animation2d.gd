class_name Animation2D extends IndexedResource

static func _type_directory() -> String:  return Paths.animations()

@export var sound     : String = ""

@export var instances : Array[AnimationInstance]

# non-int array presize
func _init() -> void:
	var arr_size : int
	
	arr_size = 2
	instances.resize(arr_size)
	for i in arr_size: instances[i] = AnimationInstance.new()
