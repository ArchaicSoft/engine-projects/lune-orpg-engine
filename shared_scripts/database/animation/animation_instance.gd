class_name AnimationInstance extends Resource

@export var sprite     : int = -1
@export var frames     : int = 0
@export var loop_count : int = 0
@export var loop_time  : int = 0
