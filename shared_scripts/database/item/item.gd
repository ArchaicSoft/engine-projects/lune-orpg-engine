class_name Item extends IndexedResource

static func _type_directory() -> String:  return Paths.items()

# basic descriptors
@export var description     : String             = "A brand new item is available."
@export var sound           : String             = ""
@export var image           : int                = -1
@export var paperdoll       : int                = -1

# mechanics
@export var type            : Globals.Item       = Globals.Item.none
@export var data_1          : int                = -1 # logic storage
@export var data_2          : int                = -1 # logic storage
@export var data_3          : int                = -1 # logic storage
@export var requirements    : PlayerRequirements = PlayerRequirements.new()

# modifiers
@export var modifies_stats  : Array[int]         = Utility.array_presize(Globals.Stat.keys().size(),  0)
@export var modifies_vitals : Array[int]         = Utility.array_presize(Globals.Vital.keys().size(), 0)

# spell
@export var casts_spell     : int                = -1
@export var casts_instant   : bool               = false

# misc
@export var price           : int                = 0
@export var rarity          : int                = 0
@export var speed           : int                = 0
@export var consume_bonus   : Array[int]         = Utility.array_presize(Globals.Vital.keys().size(), 0)
