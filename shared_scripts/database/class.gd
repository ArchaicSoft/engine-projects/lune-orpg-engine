class_name Class extends IndexedResource

static func _type_directory() -> String:  return Paths.classes()

# basic descriptors
@export var stats         : Array[int]      = Utility.array_presize(Globals.Stat.keys().size(), 0)

# sprite selection
@export var sprite_male   : Array[int]      = Utility.array_presize(1, 0)
@export var sprite_female : Array[int]      = Utility.array_presize(1, 0)

# creation (one time only) info
@export var start_map     : int             = 0
@export var start_pos_x   : float           = 0.0
@export var start_pos_y   : float           = 0.0
@export var start_items   : Array[ItemSlot]
@export var start_spells  : Array[int]

# public accessors
func get_max_vital(vital: Globals.Vital) -> int:
	match vital:
		Globals.Vital.hp: return 100 + (stats[Globals.Stat.endurance]    *  5) + 2
		Globals.Vital.mp: return  30 + (stats[Globals.Stat.intelligence] * 10) + 2
		_: return 0

func get_start_position()               -> Vector2: return Vector2(start_pos_x, start_pos_y)
func set_start_position(value: Vector2) -> void:    start_pos_x = value.x; start_pos_y = value.y
