class_name Tile extends Resource

@export var layers    : Array[int]   = Utility.array_presize(Globals.MapLayer.keys().size(), 0x00000000FFFF)
@export var type      : Globals.Tile = Globals.Tile.none
@export var data_1    : int = 0
@export var data_2    : int = 0
@export var data_3    : int = 0
@export var dir_block : int = 0

# grid accessors
func get_layer_tileset(layers_index: Globals.MapLayer) -> int: return layers[layers_index] & 0xFFFF
func get_layer_x(layers_index: Globals.MapLayer)       -> int: return (layers[layers_index] >> 0x10) & 0xFFFF
func get_layer_y(layers_index: Globals.MapLayer)       -> int: return (layers[layers_index] >> 0x20) & 0xFFFF
func get_layer_xy(layers_index: Globals.MapLayer) -> Vector2i:
	return Vector2i(get_layer_x(layers_index), get_layer_y(layers_index))

func set_layer_tileset(layers_index: Globals.MapLayer, value: int) -> void:
	var data: int = layers[layers_index] & 0xFFFFFFFF0000
	layers[layers_index] = (data | (value & 0xFFFF))

func set_layer_x(layers_index: Globals.MapLayer, value: int) -> void:
	var data: int = layers[layers_index] & 0xFFFF0000FFFF
	layers[layers_index] = (data | ((value & 0xFFFF) << 0x10))

func set_layer_y(layers_index: Globals.MapLayer, value: int) -> void:
	var data: int = layers[layers_index] & 0x0000FFFFFFFF
	layers[layers_index] = (data | ((value & 0xFFFF) << 0x20))

func set_layer_xy(layers_index: Globals.MapLayer, value: Vector2i) -> void:
	set_layer_x(layers_index, value.x)
	set_layer_y(layers_index, value.y)
