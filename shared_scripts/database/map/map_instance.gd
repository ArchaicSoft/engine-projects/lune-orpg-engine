class_name MapInstance extends Resource

@export var players  : Array[int]
@export var npcs     : Array[int]
@export var supplies : Array[int]
