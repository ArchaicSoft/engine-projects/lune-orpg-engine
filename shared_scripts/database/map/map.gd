class_name Map extends IndexedResource

static func _type_directory() -> String:  return Paths.maps()

# basic descriptors
@export var music         : String      = ""

# other maps
@export var death_map     : int         = -1
@export var death_pos     : Vector2     = Vector2.ZERO
@export var connected_map : Array[int]  = Utility.array_presize(Globals.Direction.keys().size(), -1)

# content data
@export var type          : Globals.Map = Globals.Map.none
@export var size          : Vector2i    = Vector2i(8, 8)
@export var tiles         : Array[Tile]

# public accessors
func get_tile(x: int, y: int) -> Tile:
	if x < 0 or x > size.x or y < 0 or y > size.y: return null
	return tiles[x * size.x + y]

# non-int array presize
func _init() -> void:
	var arr_size : int
	
	arr_size = size.x * size.y
	tiles.resize(arr_size)
	for i in arr_size: tiles[i] = Tile.new()
