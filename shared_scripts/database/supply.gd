class_name Supply extends IndexedResource

static func _type_directory() -> String:  return Paths.supplies()

# basic descriptors
@export var message_empty    : String                = "No Supplies available"
@export var message_reward   : String                = "Got Supply(s)"
@export var sound            : String                = ""
@export var sprite_empty     : int                   = -1 
@export var sprite_available : int                   = -1

# mechanics
@export var animation        : int                   = -1
@export var required_tool    : int                   = -1
@export var blocking_tile    : bool                  = true
@export var durability       : int                   = 1
@export var reward_intervals : int                   = 1

# misc properties
@export var rewards          : Array[ChanceItemSlot]
@export var respawn_time     : int                   = 300
