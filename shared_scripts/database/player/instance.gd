
class_name PlayerInstance extends RefCounted

var network_id         : int               = 0
var status             : Player.Status     = Player.Status.logged_out
var in_editor          : bool              = false

var move_type          : Globals.Movement  = Globals.Movement.standing
var move_speed         : float             = 0.0

# server side tracking
var request_movement   : bool              = false
var request_move_dir   : Globals.Direction = Globals.Direction.down
var request_move_type  : Globals.Movement  = Globals.Movement.standing
var request_move_timer : float             = 0.0
