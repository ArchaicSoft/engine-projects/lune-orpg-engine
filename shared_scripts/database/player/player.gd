class_name Player extends Resource

enum Status {
	logged_out,
	logged_in,
	in_game
}

@export var account   : Account
@export var character : Character
var         instance  : PlayerInstance
