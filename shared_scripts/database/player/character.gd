
class_name Character extends Resource

# basic descriptors
@export var name        : String                   = ""
@export var sprite      : int                      = -1
@export var sex         : Globals.Sex              = Globals.Sex.male
@export var class_type  : int                      = 0
@export var level       : int                      = 1
@export var stat_points : int                      = 0

# geolocation
@export var map         : int                      = 0
@export var pos_x       : float                    = 0.0
@export var pos_y       : float                    = 0.0
@export var direction   : Globals.Direction        = Globals.Direction.down

# carried object arrays
@export var equipment   : Array[ItemSlot]
@export var inventory   : Array[ItemSlot]
@export var spells      : Array[int]               = Utility.array_presize(Globals.Max.spells,         -1)

# status info
@export var stats       : Array[int]               = Utility.array_presize(Globals.Stat.keys().size(),  0)
@export var vitals      : Array[int]               = Utility.array_presize(Globals.Vital.keys().size(), 0)

# public accessors
func get_position()               -> Vector2: return Vector2(pos_x, pos_y)
func set_position(value: Vector2) -> void:    pos_x = value.x; pos_y = value.y

# non-int array presize
func _init() -> void:
	var arr_size : int
	
	arr_size = Globals.Equipment.keys().size()
	equipment.resize(arr_size)
	for i in arr_size: equipment[i] = ItemSlot.new()
	
	arr_size = Globals.Max.inventory
	inventory.resize(arr_size)
	for i in arr_size: inventory[i] = ItemSlot.new()
