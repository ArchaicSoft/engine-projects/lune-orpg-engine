class_name PlayerRequirements extends Resource

@export var requires_access : Globals.Access = Globals.Access.player
@export var requires_class  : int            = -1
@export var requires_level  : int            = -1
@export var requires_stats  : Array[int]     = Utility.array_presize(Globals.Stat.keys().size(),  0)

func meets_requirements(player: Player) -> bool:
	if player.account.access < requires_access: return false
	
	if player.character.class_type != requires_class: return false
	
	if player.character.level < requires_level: return false
	
	for i in requires_stats.size():
		if player.character.stats[i] < requires_stats[i]:
			return false
	
	# met all requirements
	return true
