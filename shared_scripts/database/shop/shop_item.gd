class_name ShopItem extends Resource

@export var receives_item := ItemSlot.new()
@export var requires_item := ItemSlot.new()
