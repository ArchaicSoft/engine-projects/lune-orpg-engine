class_name Shop extends IndexedResource

static func _type_directory() -> String:  return Paths.shops()

@export var buy_rate  : float  = 100.0

@export var inventory : Array[ShopItem]
