class_name BB_Text

# string array of all existing bb code tags godot currently supports.
const TAGS : Array[String] = [
	"b", "i", "u", "s", "p", "ul", "ol",
	"code", "center", "left", "right",
	"fill", "indent", "url", "hint",
	"img", "color", "bgcolor", "fgcolor",
	"font", "font_size",
	"outline_size", "outline_color",
	"dropcap", "opentype_features",
	"table", "cell",
	"wave", "tornado", "shake",
	"fade", "rainbow"
]

static var REGEX : RegEx = RegEx.new()

##
# Finds whitespace characters and returns a string trimmed of consecutive characters.
# Also removes all leading and trailing whitespace.
#
# Params: message : The String in which will be searched.
#
# Returns: A string in which has been trimmed of any duplicate whitespace characters.
#
# Example: trim_spaces('\tTest \t\t   Message \t\t  ') => 'Test Message'
##
static func trim_spaces(message: String) -> String:
	REGEX.clear()
	REGEX.compile("[\t ]+")
	return REGEX.sub(message, " ", true).strip_edges()

##
# Finds new line characters and connected whitespace and returns a string stripped of them.
#
# Params: message : The String in which will be searched.
#
# Returns: A String in which has been trimmed of newlines and connected whitespace.
#
# Example: trim_spaces('\tTest \t\t\r\n\n\r \n   Message \t\t  ') => '\tTest Message \t\t  '
##
static func strip_new_lines(message: String) -> String:
	REGEX.clear()
	REGEX.compile("(\r\n)[\t ]+")
	return REGEX.sub(message, "", true).strip_escapes()

##
# Finds new line characters and connected whitespace and returns a string
# which strips the connected whitespace and normalizes newlines.
#
# Params: message : The String in which will be searched.
#
# Returns: A String in which has been trimmed of whitespace and normalizes newlines.
#
# Example: trim_spaces('\tTest \t\t\r\n\n\r \n   Message \t\t  ') => '\tTest\r\n\r\nMessage \t\t  '
##
static func normalize_new_lines(message: String) -> String:
	# convert all groupings of newline and whitespace to newlines only
	REGEX.clear()
	REGEX.compile("(\r\n)[\t ]+|[\t ]+(\r\n)|[\t ]+")
	message = REGEX.sub(message, "$1$2", true) # $1$2 is potentially junk that needs changed
	
	# normalize newline character sets
	REGEX.clear()
	REGEX.compile("\r\n|\n\r|\n|\r")
	return REGEX.sub(message, "\r\n", true)

##
# Finds and returns the a String with any incorrect BBCode Tags fixed,
# or stripped.
#
# Params: message : The String in which will be searched.
#
# Returns: A String in which all BBCode Tags are properly closed.
#
# Example: clean_tags('[url]www.google.ca') => '[url]www.google.ca[/url] test'
##
static func clean_tags(message: String) -> String:
	REGEX.clear()
	REGEX.compile(r"\[(\w+)(?:=[^\]]+)?](?![\s\S]*\[/\1])")
	
	# add closing tags
	var matches := REGEX.search_all(message)
	for i in range(matches.size() - 1, -1, -1):
		message += "[/" + matches[i].get_string() + "]"
	
	return message

# TODO: THIS AND UP

##
# Finds and returns the a String with any specified bracket enclosed tags removed.
#
# Params:
#         message : The String in which will be searched.
#         tags    : The Array of bracket enclosed tags '[]' which are not required to be BBCode.
#
# Returns: A String in which has been trimmed of requested Tags.
#
# Example: strip_tags('[url]www.google.ca[/url]') => 'www.google.ca'
##
static func strip_tags(message: String, tags: Array[String]) -> String:
	REGEX.clear()
	REGEX.compile(r"\[(" + "|".join(tags) + r")(=[^\]]+)?\](.*?)\[\/\1\]|\[(" + "|".join(tags) + r")(=[^\]]+)?\]")
	return REGEX.sub(message, "", true)

##
# Finds and returns the a String with any BBCode Tags removed.
#
# Params: message : The String in which will be searched.
#
# Returns: A String in which has been trimmed of all BBCode Tags.
#
# Example: strip_bb_code('[url]www.google.ca[/url]') => 'www.google.ca'
##
static func strip_bb_code(message: String) -> String:
	return strip_tags(message, TAGS)

##
# Finds any text or non bb-code tags in the provided string.
#
# Params: message : The String in which will be searched.
#
# Returns: True if the BBCode contains text.
#
# Example: has_text_between_tags('[url]www.google.ca[/url]') => true
##
static func has_text_between_tags(message: String) -> bool:
	REGEX.clear()
	REGEX.compile(r"\[([^\s\/\]=]+)[^\]]*\](?!\s*\[)[^\[]*\S[^\[]*\[\/\1\]")
	
	return REGEX.search(message) != null

##
# Finds any positive text ignoring the presence of tags.
#
# Params: message : The String in which will be searched.
#
# Returns: True if the string contains non-tag/non-whitespace text.
#
# Example: has_text_excluding_tags('hello [color] world [/color]') => true
##
static func has_text_excluding_tags(message: String) -> bool:
	REGEX.clear()
	REGEX.compile(r"\[[^\]]*\]|\S")
	
	return REGEX.search(message) != null
