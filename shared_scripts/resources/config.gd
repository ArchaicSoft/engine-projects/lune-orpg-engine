class_name ConfigResource extends Resource

# internal data

static func _get_file_path(name: String) -> String:
	return Paths.config() + name + ".tres"

# override property

func _name() -> String: return "ConfigResource"

# public accessors

func type_name() -> String:
	return self.get_script().get_path().get_file().get_basename()

func load_self() -> ConfigResource:
	return load_file(self)

func save_self() -> void:
	save_file(self)

static func load_file(cfg: ConfigResource) -> ConfigResource:
	var ret = Utility.load_resource(_get_file_path(cfg._name()))
	return cfg if ret == null else ret

static func save_file(cfg: ConfigResource) -> void:
	Utility.save_resource(cfg, _get_file_path(cfg._name()))
