class_name IndexedResource extends Resource

# internal data

static func _type_directory() -> String: return Paths.database()

static func _get_file_path(index: int) -> String:
	return _type_directory() + str(index) + ".tres"

# re-used properties

var name     : String = "New " + type_name()
var revision : int    = 0

# public accessors

func type_name() -> String:
	return self.get_script().get_path().get_file().get_basename()

func load_self(index: int) -> IndexedResource:
	var ret = load_file(self, index)
	return self if ret == null else ret

func save_self(index: int) -> void:
	save_file(self, index)

static func load_file(db: IndexedResource, index: int) -> IndexedResource:
	@warning_ignore ("STATIC_CALLED_ON_INSTANCE")
	return Utility.load_resource(db._get_file_path(index), "", ResourceLoader.CACHE_MODE_IGNORE)

static func save_file(db: IndexedResource, index: int) -> void:
	Utility.save_resource(db, _get_file_path(index))
