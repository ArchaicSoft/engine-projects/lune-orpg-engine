class_name SharedDatabase

static var animations : Array[Animation2D]
static var classes    : Array[Class]
static var items      : Array[Item]
static var maps       : Array[Map]
static var npcs       : Array[Npc]
static var shops      : Array[Shop]
static var spells     : Array[Spell]
static var supplies   : Array[Supply]

# non-int array presize
static func _init() -> void:
	var arr_size : int
	
	arr_size = Globals.Max.animations
	animations.resize(arr_size)
	for i in arr_size: animations[i] = Animation2D.new()
	
	arr_size = Globals.Max.classes
	classes.resize(arr_size)
	for i in arr_size: classes[i] = Class.new()
	
	arr_size = Globals.Max.items
	items.resize(arr_size)
	for i in arr_size: items[i] = Item.new()
	
	arr_size = Globals.Max.maps
	maps.resize(arr_size)
	for i in arr_size: maps[i] = Map.new()
	
	arr_size = Globals.Max.npcs
	npcs.resize(arr_size)
	for i in arr_size: npcs[i] = Npc.new()
	
	arr_size = Globals.Max.shops
	shops.resize(arr_size)
	for i in arr_size: shops[i] = Shop.new()
	
	arr_size = Globals.Max.spells
	spells.resize(arr_size)
	for i in arr_size: spells[i] = Spell.new()
	
	arr_size = Globals.Max.supplies
	supplies.resize(arr_size)
	for i in arr_size: supplies[i] = Supply.new()
	
