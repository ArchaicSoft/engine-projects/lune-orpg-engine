class_name Globals

# constants

const MIN_USERNAME             : int    = 4
const MAX_USERNAME             : int    = 30
const MIN_PASSWORD             : int    = 7
const MAX_PASSWORD             : int    = 50
const MIN_CHARACTER_NAME       : int    = 4
const MAX_CHARACTER_NAME       : int    = 30
const MAX_CHAT_INPUT           : int    = 150

const CHAT_COLOR_PLAYER        : String = "white"
const CHAT_COLOR_MODERATOR     : String = "yellow"
const CHAT_COLOR_DEVELOPER     : String = "blue"
const CHAT_COLOR_ADMINISTRATOR : String = "purple"

const CHAT_COLOR_SAY           : String = "white"
const CHAT_COLOR_BROADCAST     : String = "aqua"
const CHAT_COLOR_EMOTE         : String = "antique_white"
const CHAT_COLOR_PRIVATE       : String = "purple"
const CHAT_COLOR_HELP          : String = "yellow"
const CHAT_COLOR_WHO           : String = "orange"
const CHAT_COLOR_JOIN_LEAVE    : String = "dim_gray"
const CHAT_COLOR_NPC           : String = "burlywood"
const CHAT_COLOR_ALERT         : String = "red"
const CHAT_COLOR_NEW_MAP       : String = "lemon_chiffon"

const CHAT_CHAR_COMMAND        : String = '/'
const CHAT_CHAR_BROADCAST      : String = '\''
const CHAT_CHAR_EMOTE          : String = '-'
const CHAT_CHAR_PRIVATE        : String = '!'

const CHAT_COMMAND_HELP        : String = "/help"

const INPUT_OPEN_CONTROL_PANEL :        = KEY_INSERT

const MOVE_SPEED_WALKING       : float = 0.7
const MOVE_SPEED_RUNNING       : float = 0.25

const TILE_SIZE                : int    = 32

# enumerators

enum Access {
	player,
	moderator,
	developer,
	administrator
}

enum Chat {
	broadcast,
	emote,
	private,
	say
}

enum Direction {
	up,
	left,
	right,
	down
}

enum Editor {
	animation,
	item,
	map,
	npc,
	shop,
	spell,
	supply
}

enum Equipment {
	helmet,
	torso,
	shield,
	weapon
}

enum Item {
	none,
	currency,
	key,
	consume,
	spell,
	equipment
}

enum Map {
	none,
	safe
}

enum MapLayer {
	ground,
	mask1,
	mask2,
	fringe1,
	fringe2
}

enum Max {
	animations    = 50,
	bank          = 50,
	classes       = 5,
	hotbar        = 10,
	inventory     = 30,
	items         = 50,
	maps          = 100,
	npcs          = 50,
	party_members = 4,
	player_spells = 10,
	shops         = 10,
	spells        = 30,
	supplies      = 20
}

enum Movement {
	standing,
	walking,
	running,
	attacking
}

enum Npc {
	friendly,
	shop,
	guard,
	manual_aggro,
	auto_aggro
}

enum Sex {
	male,
	female
}

enum Spell {
	status_mod,
	status_buff,
	warp
}

enum Stat {
	strength,
	endurance,
	intelligence,
	agility,
	willpower,
}

enum Target {
	none,
	player,
	npc
}

enum Tile {
	none,
	block,
	item,
	bank,
	shop,
	slide,
	warp,
	vital_mod,
	lock,
	unlock,
	door,
	supply,
	npc_avoid,
	npc_spawn
}

enum Vital {
	hp,
	mp,
	xp
}
