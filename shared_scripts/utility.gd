class_name Utility

#######################################
### Array Functions
##############################

static func array_presize(size: int, fill: int) -> Array[int]:
	var array : Array[int]
	array.resize(size)
	
	for i in size:
		array[i] = fill
	
	return array

#######################################
### IO Functions
##############################

##
# Ensures a directory exists at the specified path
#
# Params:
#         path : The directory string.
#
# Example:
#         check_path('user://my_path') => creates if missing "@:Godot/my_path".
##
static func check_path(path: String) -> void:
	path = path.get_base_dir() # correct path for directory only
	
	if DirAccess.dir_exists_absolute(path): return
	DirAccess.make_dir_recursive_absolute(path)

##
# Tests a String to determine is it contains Alpha Numeric Characters or not.
#
# Params:
#         text : The text to test against.
#
# Example:
#         is_alpha_numeric('Test1234') => true
##
static func is_alpha_numeric(text: String) -> bool:
	var regex = RegEx.new()
	regex.compile("[^A-Za-z0-9]")
	return not regex.search(text)

##
# Echoes ResourceLoader.load() with safety.
##
static func load_resource(
	path: String,
	type_hint: String = "",
	cache_mode: ResourceLoader.CacheMode = ResourceLoader.CACHE_MODE_REUSE
) -> Resource:
	check_path(path)
	if not FileAccess.file_exists(path): return null
	return ResourceLoader.load(path, type_hint, cache_mode)

##
# Echoes ResourceSaver.Save() with safety.
##
static func save_resource(
	resource: Resource,
	path: String = "",
	flags: ResourceSaver.SaverFlags = ResourceSaver.FLAG_NONE
) -> Error:
	check_path(path)
	return ResourceSaver.save(resource, path, flags)

#######################################
### Logging Functions
##############################

##
# The backend of the other logging functions. Creates a log message in
# either a current log file with timestamps or a timestamped file.
##
static func _write_log(message: String, path: String, concat_today: bool) -> void:
	# adjust path
	path = Paths.logs() + path
	
	# ensure directory exists
	Utility.check_path(path)
	
	# incorporate time
	if concat_today:
		path    = Time.get_date_string_from_system()
		message = Time.get_time_string_from_system() + ": " + message
	else:
		path    = Time.get_datetime_string_from_system()
	
	# print if in debugging mode
	if OS.is_debug_build():
		print(message)
	
	# perform io
	var file := FileAccess.open(path, FileAccess.READ_WRITE)
	
	if not file: # file not accessible
		print("Logging file failed, attempt blocked!")
		return
	
	file.seek_end()
	file.store_line(message)
	file.close()

##
# Creates a Log Message that will be wrote to a File within the Activity Logs folder.
# Activity Logs are non error events that you want documented for historical record keeping.
# This Function has the option to Add the Current Time to the Log Messsage, or not.
# This Log File will save with the File Name of the Current Date.
#
# Params:
#         message      : The Message to log.
#         concat_today : Append Current Time to the log message or just Current Date to the File Name.
#
# Example:
#         log_activity('[Player] made the announcement: We have special event in 3 days.', true)
##
static func log_activity(message: String, concat_today: bool = true) -> void:
	_write_log(message, "activity/", concat_today)

##
# Creates a Log Message that will be wrote to a File within the Error Logs folder.
# This Function has the option to Add the Current Time to the Log Messsage, or not.
# This Log File will save with the File Name of the Current Date.
#
# Params:
#         message      : The Message to log.
#         concat_today : Append Current Time to the log message or just Current Date to the File Name.
#
# Example:
#         log_error('Player object was null!', true)
##
static func log_error(message: String, concat_today: bool = true) -> void:
	_write_log(message, "errors/", concat_today)

##
# Creates a Log Message that will be wrote to a File within the Hacking Logs folder.
# Hacking Logs are known or suspected hacking attempts which fall under the category
# of blatant cheating, attacking of the network connection, or suspicious activity
# which can occur from data being way out of expected boundaries.
# Suspicious activity could look like packets sent at invalid times, character state
# not matching a request, or packet request timing being too frequent when coded at spaced intervals.
# This Function has the option to Add the Current Time to the Log Messsage, or not.
# This Log File will save with the File Name of the Current Date.
#
# Params:
#         message      : The Message to log.
#         concat_today : Append Current Time to the log message or just Current Date to the File Name.
#
# Example:
#         log_hacking('Packet flooding from [ip].', true)
##
static func log_hacking(message: String, concat_today: bool = true) -> void:
	_write_log(message, "hacking/", concat_today)
