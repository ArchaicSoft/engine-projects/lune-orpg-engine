class_name Packet

#################################
## Packets sent by the Client, ##
## and received by the Server. ##
#################################

enum Client {
	# pre game management
	login,
	register,
	character_create,
	character_select,
	character_delete,
	
	# in game functionality
	chat,
	request_move,
	
	# editor states
	editor_opened,
	editor_closed,
	editor_saved,
	
	#####################
	## player commands ##
	####################
	
	command_help,
	
	####################
	## admin commands ##
	####################
	
	# general
	
	command_save_database,
	command_delete_bans,
	
	# modify player
	command_change_sprite,
	command_add_level,
	command_set_access,
	command_kick,
	command_ban,
	
	# map stuff
	command_warp_to,
	command_warp_to_me,
	command_warp_me_to,
	command_refresh_map
}

#################################
## Packets sent by the Server, ##
## and received by the Client. ##
#################################

enum Server {
	alert,
	
	# pre game management
	game_data,
	update_data,
	enter_account,
	enter_game,
	leave_game,
	
	# in game functionality
	change_map,
	present_map,
	
	player_join_map,
	player_left_map,
	player_update,
	player_move,
	player_access,
	player_sprite,
	player_equipment,
	
	chat
}
