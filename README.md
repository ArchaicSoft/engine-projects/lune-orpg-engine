## Lune Online Role Playing Game Engine (LORPG)

This is a remake of the Eclipse Origins 2.0 (Formerly Mirage Source) ORPG engine 
using the Godot 4.X(+) Game Engine as an IDE and for its [easier/more versatile] 
GDScript programming language.

It offers a few benefits such as:
  - Future proofing the development environment (Unlike the old/dead/illegal VB6)
  - Expanding feature sets
  - Allowing for more advanced featuresets
  - Performance improvements (especially in networking)
  - Built in optional compression and encryption
  - Cross Platform Support (Mac/Linux and Mobiles) without windows emulation
  - Web-Host Integration support

This project began to accomplish a few tasks:
  - Prove my capability for a future development teams
  - Rescue older VB6 projects (easy migration)
  - Be easy to learn and work in (comparatively to VB6/Eclipse)

# Completion Goal

Upon completion this engine should work identically to Eclipse Origins 2.0 
in all its featureset with minimal additional features, while maintaining 
unique assets and removing any spooky licensing or infringing done by previous 
eclipse edition releases with rpgmaker assets.

# Plans after completion

After this project meets its conclusion, the goal is to hand over 'official' 
development to a couple of people that are trusted to keep the spirit of the 
project alive. The 'completed' project will be pushed to a branch called 1.0 (eclipse) 
and future/main branches will continue to add 'out of the box' changes and additional 
features to make a more rounded engine than the original EO2.0 base (as future eclipse 
editions have done -- eg: rpgmaker-esque event editor, player housing, multiple characters, etc).