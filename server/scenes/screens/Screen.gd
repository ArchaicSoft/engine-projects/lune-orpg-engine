extends Node

@onready var _text  : RichTextLabel = $Text/Buffer
@onready var _input : LineEdit      = $Input/Text

func _ready() -> void:
	Console.clear = gui_clear
	Console.write = gui_write
	Console.error = gui_error

func gui_clear() -> void:
	_text.clear()
	_text.append_text("\n[center][b][color=purple] LORPG[/color][color=yellow] SERVER [/color][/b][/center]\n\n")
	Console.cli_clear()

func gui_write(line: String) -> void:
	_text.append_text(line + "\n")
	Console.cli_write(line)

func gui_error(line: String) -> void:
	_text.append_text("[b][color=red]ERROR:[/color][/b] " + line + "\n")

func _on_input_text_submitted(new_text: String) -> void:
	var message : String = new_text.strip_escapes().strip_edges(true, true)
	if message.length() < 1: return # must have real text to input
	
	# sent a message
	if not message.begins_with("/"):
		# display plain message to console
		Console.write.call("Server Say: " + message)
		
		# display to all players
		Network.send_server_message("[rainbow]Server: [/rainbow]" + message)
	
	# handle commands
	else:
		PlayerList.display()
	# pass
	
	_input.clear()
