extends RdpServer

##
# Provides access to log debug information in both backend and frontend.
##
func log_debug(message: String) -> void:
	if not Session.options.debugging: return
	Console.write.call(message)

##
# Provides access to log information in both backend and frontend.
##
func log_status(message: String) -> void:
	Console.write.call(message)

##
# User-Defined event for interacting on a peer connect signal.
#
# Params:
#         id : ENet socket connetion uuid.
##
func _peer_connected(id: int) -> void:
	var player                := Player.new()
	player.instance            = PlayerInstance.new()
	player.instance.network_id = id
	Session.players[id]        = player
	
	# add to gui
	PlayerList.add_player(id)

##
# User-Defined event for interacting on a peer disconnect signal.
#
# Params:
#         id : ENet socket connetion uuid.
##
func _peer_disconnected(id: int) -> void:
	var player : Player = Session.get_player(id)
	
	# check if in-game in to save data
	if player.character != null:
		Session.save_account(id)
		
		# remove from active users
		var username : String = player.account.username
		var index    : int    = Session.active_accounts.find(username)
		Session.active_accounts.remove_at(index)
		
		# notify players on map they are leaving
		send_server_message_to_map(
			player.character.map, 
			"[color=%s]%s[/color] has left the game." %\
			[Session.get_access_color(id), player.character.name],
			Globals.CHAT_COLOR_JOIN_LEAVE
		)
		
		# officially remove from map
		send_player_left_map(id)
		send_leave_game(id)
	
	# remove from player list
	Session.players.erase(id)
	
	# remove from gui
	PlayerList.remove_player(id)

##
# Sends a Packet to all 'in-game' clients.
#
# Params:
#         header     : The Packet Header defined in the Packet.Server Enum.
#         args       : The Packet data or arguments if any.
#         unreliable : Opts to allow unreliable transport. Good for Speed, Dangerous for Syncrosity. False by default.
##
func socket_send_to_all(
	header     : Packet.Server,
	args       : Array = [],
	unreliable : bool  = false
) -> void:
	for pid in Session.players.keys():
		if Session.get_player(pid).account != null: # in game check
			socket_send_to(pid, header, args, unreliable)

##
# Sends a Packet to all 'in-game' clients except the specified Enet Socket Connect uuid.
#
# Params:
#         id         : The target clients ENet socket connetion uuid.
#         header     : The Packet Header defined in the Packet.Server Enum.
#         args       : The Packet data or arguments if any.
#         unreliable : Opts to allow unreliable transport. Good for Speed, Dangerous for Syncrosity. False by default.
##
func socket_send_to_all_but(
	id         : int,
	header     : Packet.Server,
	args       : Array = [],
	unreliable : bool  = false
) -> void:
	for pid in Session.players.keys():
		if pid != id and Session.get_player(pid).account != null: # in game check
			socket_send_to(pid, header, args, unreliable)

##
# Sends a Packet to all 'in-game' clients on specified map id.
#
# Params:
#         map_index  : The target map to send data to.
#         header     : The Packet Header defined in the Packet.Server Enum.
#         args       : The Packet data or arguments if any.
#         unreliable : Opts to allow unreliable transport. Good for Speed, Dangerous for Syncrosity. False by default.
##
func socket_send_to_map(
	map_index  : int,
	header     : Packet.Server,
	args       : Array = [],
	unreliable : bool  = false
) -> void:
	var map_inst : MapInstance = Session.get_map_instance(map_index)
	
	# scan all players in map and send them packet
	for pid in map_inst.players:
		socket_send_to(pid, header, args, unreliable)

##
# Sends a Packet to all 'in-game' clients on specified map id.
#
# Params:
#         id         : The target clients ENet socket connetion uuid.
#         map_index  : The target map to send data to.
#         header     : The Packet Header defined in the Packet.Server Enum.
#         args       : The Packet data or arguments if any.
#         unreliable : Opts to allow unreliable transport. Good for Speed, Dangerous for Syncrosity. False by default.
##
func socket_send_to_map_but(
	id         : int,
	map_index  : int,
	header     : Packet.Server,
	args       : Array = [],
	unreliable : bool  = false
) -> void:
	var map_inst : MapInstance = Session.get_map_instance(map_index)
	
	# scan all players in map and send them packet
	for pid in map_inst.players:
		if pid != id:
			socket_send_to(pid, header, args, unreliable)

#######################################
### Receive Security Functions
### Every redundant check for authentication or proper game state that is shared
### at minimum between 2 functions belongs here so security checks can be properly
### applied with consistency and clarity
##############################

func hacking_status_attempt(caller: String, id: int, status: Player.Status) -> bool:
	var player : Player = Session.get_player(id)
	
	# check disconnection
	if player == null:
		return true # invalid state
	
	if player.instance.status != status:
		var message := "[status hack] " + caller
		if player.account.username:
			message = "%s: %s" % [player.account.username, message]
		Utility.log_hacking(message)
		
		return true # attempted hacking
	
	return false # valid state

func hacking_access_attempt(caller: String, id: int, access: Globals.Access) -> bool:
	var player : Player = Session.get_player(id)
	
	if int(player.account.access) < int(access):
		var message := "[access hack] " + caller
		if player.account.username:
			message = "%s: %s" % [player.account.username, message]
		Utility.log_hacking(message)
		
		return true # attempted hacking
	
	return false # valid state

func hacking_account_details_attempt(caller: String, username: String, password: String) -> bool:
	if not (
		Globals.MIN_USERNAME <= username.length() and
		Globals.MAX_USERNAME >= username.length() and
		Globals.MIN_PASSWORD <= password.length() and
		Globals.MAX_PASSWORD >= password.length()
	):
		Utility.log_hacking("[account details hack] " + caller)
		return true # attempted hacking
	
	return false # valid state

func hacking_character_details_attempt(caller: String, id: int, char_name: String, class_type: int) -> bool:
	var player : Player = Session.get_player(id)
	
	if not (
		Globals.MIN_CHARACTER_NAME <= char_name.length() and
		Globals.MAX_CHARACTER_NAME >= char_name.length() and
		Utility.is_alpha_numeric(name) and
		0 <= class_type and class_type <= Globals.Max.classes
	):
		var message := "%s: [character details hack] %s" % [player.account.username, caller]
		Utility.log_hacking(message)
		return true # attempted hacking
	
	return false # valid state

func hacking_character_attempt(caller: String, id: int, character_exists: bool) -> bool:
	var player : Player = Session.get_player(id)
	
	if (player.character != null) != character_exists:
		var message := "%s: [character hack] %s" % [player.account.username, caller]
		Utility.log_hacking(message)
		
		return true # attempted hacking
	
	return false # valid state

#######################################
### Receive Functions
### Every Packet specified in Packet.Client enum must be specified here to be handled when received from the client.
### You must put 'receive_' prior to the Packet Header name to denote a Receive Handler Function.
### Parameters of this handler function must be the same as you'd expect to be received as args IF ANY.
##############################

func receive_login(username: String, password: String, id: int) -> bool:
	# ensure strings arent actually empty-bypasses
	username = username.strip_escapes().to_lower()
	password = password.strip_escapes()
	
	# hacking attempt
	var caller : String = "receive_login"
	if (
		hacking_status_attempt(caller, id, Player.Status.logged_out) or
		hacking_account_details_attempt(caller, username, password)
	): return false # force disconnect
	
	# make sure account isn't already logged in
	if Session.active_accounts.has(username):
		send_alert(
			id, "Account is already logged in. If you have recently " +\
			"disconnected please wait a few minutes to try again. " +\
			"If you have not logged in recently, please report to " +\
			"an administrator immediately to get your account " +\
			"re-secured!",
			"Login failed"
		)
		return true # successfully received
	
	var tmp_player : Player = Session.load_account(username)
	
	# make sure an account was retrieved
	if tmp_player == null or tmp_player.account == null:
		send_alert(id, "Account does not exist!", "Login failed")
		return true # successfully received
	
	# ensure password verifies them
	if password != tmp_player.account.password:
		send_alert(id, "Password is invalid!", "Login failed")
		return true # successfully received
	
	# adjustment ensures username matches file name
	tmp_player.account.username = username
	
	# successful attempt, send them to account screen
	Session.process_login_successful(id, tmp_player)
	
	return true # successfully received

func receive_register(username: String, password: String, id: int) -> bool:
	# ensure strings arent actually empty-bypasses
	username = username.strip_escapes().to_lower()
	password = password.strip_escapes()
	
	# hacking attempt
	var caller : String = "receive_register"
	if (
		hacking_status_attempt(caller, id, Player.Status.logged_out) or
		hacking_account_details_attempt(caller, username, password)
	): return false # force disconnect
	
	# ensure no invalid characters in username
	if not Utility.is_alpha_numeric(username):
		send_alert(id, "Only alpha-numeric characters are allowed for username!", "Registration failed")
		return true
	
	if not Session.create_account(username, password):
		send_alert(id, "Account already exists! Registration failed!", "Registration failed")
		return true
	
	# successful attempt, send them to account screen
	Session.process_login_successful(id, Session.load_account(username))
	
	return true # successfully received

func receive_character_create(
	char_name    : String,
	sex          : Globals.Sex,
	class_type   : int,
	sprite_index : int,
	id           : int
) -> bool:
	# hacking attempt
	var caller : String = "receive_character_create"
	if (
		hacking_status_attempt(caller, id, Player.Status.logged_in) or
		hacking_character_details_attempt(caller, id, char_name, class_type) or
		hacking_character_attempt(caller, id, false)
	): return false # force disconnect
	
	# cache some objects
	var class_info := Database.classes[class_type]
	var character  := Character.new()
	
	# correct potentially broken sprite index
	var sprite_array : Array[int]
	match sex:
		Globals.Sex.male:   sprite_array = class_info.sprite_male
		Globals.Sex.female: sprite_array = class_info.sprite_female
	if   sprite_index < 0: sprite_index = 0
	elif sprite_index >= sprite_array.size(): sprite_index = 0
	
	# assign some data to the new character
	character.name       = char_name # todo: MAKE SURE ITS UNIQUE
	character.sprite     = sprite_array[sprite_index]
	character.sex        = sex
	character.class_type = class_type
	
	# set start info
	character.map      = class_info.start_map
	character.pos_x    = class_info.start_pos_x
	character.pos_y    = class_info.start_pos_y
	for i in class_info.start_items.size():
		character.inventory[i] = class_info.start_items[i].duplicate()
	for i in class_info.start_spells.size():
		character.spells[i] = class_info.start_spells[i]
	
	# assign character player and save
	Session.get_player(id).character = character
	Session.save_account(id)
	
	# tell them it was successful and redirect them
	send_alert(id, "Character successfully created.", "Character Creation Request")
	send_enter_account(id)
	
	return true # successfully received

func receive_character_select(id: int) -> bool:
	# hacking attempt
	var caller : String = "receive_character_select"
	if (
		hacking_status_attempt(caller, id, Player.Status.logged_in) or
		hacking_character_attempt(caller, id, true)
	): return false # force disconnect
	
	# successful request, redirect them to game
	send_enter_game(id)
	
	return true # successfully received

func receive_character_delete(id: int) -> bool:
	# hacking attempt
	var caller : String = "receive_character_delete"
	if (
		hacking_status_attempt(caller, id, Player.Status.logged_in) or
		hacking_character_attempt(caller, id, true)
	): return false # force disconnect
	
	# delete character and save
	var player : Player = Session.get_player(id)
	player.character = null
	Session.save_account(id)
	
	# tell them it was successful and redirect them
	send_alert(id, "Character successfully deleted.", "Character Deletion Request")
	send_enter_account(id)
	
	return true # successfully received

func receive_chat(message: String, id: int) -> bool:
	# hacking attempt
	var caller : String = "receive_chat"
	if (
		hacking_status_attempt(caller, id, Player.Status.in_game) or 
		message.length() > Globals.MAX_CHAT_INPUT
	): return false # force disconnect
	
	# success based on send results
	return send_chat(id, message)

func receive_request_move(
	request_move : bool,
	request_dir  : Globals.Direction,
	request_type : Globals.Movement,
	id: int
) -> bool:
	# hacking attempt
	var caller : String = "receive_request_move"
	if (
		hacking_status_attempt(caller, id, Player.Status.in_game)
	): return false # force disconnect
	
	var player : Player = Session.get_player(id)
	player.instance.request_movement = request_move
	
	# end changes
	if not request_move: return true
	
	player.instance.request_move_dir  = request_dir
	player.instance.request_move_type = request_type
	
	return true # successfully received

func receive_editor_opened(id: int) -> bool:
	# hacking attempt
	var caller : String = "receive_editor_opened"
	if (
		hacking_status_attempt(caller, id, Player.Status.in_game) or 
		hacking_access_attempt(caller, id, Globals.Access.developer)
	): return false # force disconnect
	
	var player_inst : PlayerInstance = Session.get_player(id).instance
	
	# must not have been in editor
	if player_inst.in_editor: return false
	
	player_inst.in_editor = true
	
	return true # successfully received

func receive_editor_closed(id: int) -> bool:
	# hacking attempt
	var caller : String = "receive_editor_opened"
	if (
		hacking_status_attempt(caller, id, Player.Status.in_game) or 
		hacking_access_attempt(caller, id, Globals.Access.developer)
	): return false # force disconnect
	
	var player_inst : PlayerInstance = Session.get_player(id).instance
	
	# must have been in editor
	if not player_inst.in_editor: return false
	
	player_inst.in_editor = false
	
	return true # successfully received

func receive_editor_saved(
	type     : Globals.Editor,
	index    : int,
	data_raw : String,
	id       : int
) -> bool:
	# hacking attempt
	var caller : String = "receive_editor_opened"
	if (
		hacking_status_attempt(caller, id, Player.Status.in_game) or 
		hacking_access_attempt(caller, id, Globals.Access.developer)
	): return false # force disconnect
	
	var data : IndexedResource
	
	match type:
		Globals.Editor.animation:
			if index < 0 or index >= Globals.Max.animations: return false # force disconnect
			data                       = str_to_var(data_raw) as Animation2D
			Database.animations[index] = data
		
		Globals.Editor.item:
			if index < 0 or index >= Globals.Max.items:      return false # force disconnect
			data                       = str_to_var(data_raw) as Item
			Database.items[index]      = data
		
		Globals.Editor.map:
			if index < 0 or index >= Globals.Max.maps:       return false # force disconnect
			data                       = str_to_var(data_raw) as Map
			Database.maps[index]       = data
		
		Globals.Editor.npc:
			if index < 0 or index >= Globals.Max.npcs:       return false # force disconnect
			data                       = str_to_var(data_raw) as Npc
			Database.npcs[index]       = data
		
		Globals.Editor.shop:
			if index < 0 or index >= Globals.Max.shops:      return false # force disconnect
			data                       = str_to_var(data_raw) as Shop
			Database.shops[index]      = data
		
		Globals.Editor.spell:
			if index < 0 or index >= Globals.Max.spells:     return false # force disconnect
			data                       = str_to_var(data_raw) as Spell
			Database.spells[index]     = data
		
		Globals.Editor.supply:
			if index < 0 or index >= Globals.Max.supplies:   return false # force disconnect
			data                       = str_to_var(data_raw) as Supply
			Database.supplies[index]   = data
		
		_: return false # force disconnect
	
	# save and cache the updates
	data.save_self(index)
	Session.database_cache[data.type_name()][index] = data_raw
	
	# send them confirmation of a success
	var response : String = "%s {%d} saved successfully!" % [Globals.Editor.keys()[type].capitalize(), index + 1]
	send_alert(id, response, "Editor Saved")
	
	# give all active clients the updated data
	send_update_data(type, index, data_raw)
	
	return true # successfully received

#######################################
### Send Functions
### Proper standard dictates any functionionality that requests data to
### send over the network is done so through an explicit function specified here.
### The format is 'send_' followed by a meeningful name for the type of send.
### As long as the parameters being sent remains the same, multiple functions may
### be specified to send the same packet with different configurations of data.
### Send commands must end with an array of parameter data as expected to be
### received on client.
##############################

func send_alert(id: int, message: String, title: String = "Server Alert") -> void:
	socket_send_to(id, Packet.Server.alert, [message, title])

func send_server_message(message: String, color: String = "white") -> void:
	message = Session.create_chat_message(message, color)
	if message.length() < 1: return
	socket_send_to_all(Packet.Server.chat, [message], true)

func send_server_message_to_player(id: int, message: String, color: String = "white") -> void:
	message = Session.create_chat_message(message, color)
	if message.length() < 1: return
	socket_send_to(id, Packet.Server.chat, [message], true)

func send_server_message_to_map(index: int, message: String, color: String = "white", ) -> void:
	message = Session.create_chat_message(message, color)
	if message.length() < 1: return
	socket_send_to_map(index, Packet.Server.chat, [message], true)

func send_game_data(id: int) -> void:
	socket_send_to(id, Packet.Server.game_data, [Session.database_cache])

func send_update_data(
	type     : Globals.Editor,
	index    : int,
	data_raw : String
) -> void:
	socket_send_to_all(Packet.Server.update_data, [type, index, data_raw])

func send_enter_account(id: int) -> void:
	var player_raw : String = var_to_str(Session.get_player(id))
	socket_send_to(id, Packet.Server.enter_account, [player_raw])
	
	# update player info on gui
	PlayerList.update_player(id)

func send_enter_game(id: int) -> void:
	var player : Player  = Session.get_player(id)
	
	# set them to in game status
	player.instance.status = Player.Status.in_game
	
	# send them into the game screen
	socket_send_to(id, Packet.Server.enter_game)
	
	# tell them about the map they're on
	send_change_map(id, player.character.map, true)
	
	# update player info on gui
	PlayerList.update_player(id)

# notify everyone a player left
# this makes them remove player from list
func send_leave_game(id: int) -> void:
	socket_send_to_all(Packet.Server.leave_game, [id])

# invoke a map change
# this makes client clear everything on the map,
# then notifies all cur map players of map left
# then notifies all new map players of map join
# then tells the player all current map objects
# then tells the player to present the map
func send_change_map(id: int, map_index: int, joining_game: bool = false) -> void:
	# notify players on map if they are joining
	if joining_game: 
		var player : Player = Session.get_player(id)
		send_server_message_to_player( # welcome the player
			id, Session.options.welcome,
			Globals.CHAT_COLOR_BROADCAST
		)
		send_server_message_to_map( # tell others they joined
			player.character.map,
			"[color=%s]%s[/color] has joined the game." %\
			[Session.get_access_color(id), player.character.name],
			Globals.CHAT_COLOR_JOIN_LEAVE
		)
	
	# notify everyone else player left
	else: send_player_left_map(id)
	
	# notify to clear the map and prepare for new contents
	socket_send_to(id, Packet.Server.change_map)
	
	# send everyone on new map the updated player info
	send_player_join_map(id, map_index)
	
	# todo: send some map objects here
	#
	
	# notify player theyre done loading everything
	send_present_map(id)

# invoke after a player is done loading all new map content
func send_present_map(id: int) -> void:
	socket_send_to(id, Packet.Server.present_map)

# invoke when a player changes map to tell everyone else
func send_player_join_map(id: int, map_index: int) -> void:
	var player   : Player      = Session.get_player(id)
	var map_inst : MapInstance = Session.get_map_instance(map_index)
	
	# change their map and add to map instance
	player.character.map = map_index
	map_inst.players.append(id)
	
	# duplicate player object
	var tmp_player : Player = str_to_var(var_to_str(player))
	
	# remove sensitive data
	tmp_player.account.username = ""
	tmp_player.account.password = ""
	
	# send them their updated info
	socket_send_to(id, Packet.Server.player_join_map, [var_to_str(tmp_player), id])
	
	# remove unnecessary info
	tmp_player.character.inventory.clear()
	tmp_player.character.spells.clear()
	
	# send this to everyone else
	socket_send_to_map_but(id, map_index, Packet.Server.player_join_map, [var_to_str(tmp_player), id])
	
	# send player everyone else's info
	for pid in map_inst.players:
		if pid == id: continue # dont send their own info again
		
		# get pid player object
		tmp_player = str_to_var(var_to_str(Session.get_player(pid)))
		
		# remove sensitive data
		tmp_player.account.username = ""
		tmp_player.account.password = ""
		
		# remove unnecessary info
		tmp_player.character.inventory.clear()
		tmp_player.character.spells.clear()
		
		# send info
		socket_send_to(id, Packet.Server.player_join_map, [var_to_str(tmp_player), pid])

# invoke before changing their map
# this is just to remove nodes from client side world
func send_player_left_map(id: int) -> void:
	var player  : Player = Session.get_player(id)
	var cur_map : int    = player.character.map
	
	# remove from old map instance
	Session.get_map_instance(cur_map).players.erase(id);
	
	# send this to everyone except themselves
	socket_send_to_map_but(id, cur_map, Packet.Server.player_left_map, [id])

func send_player_update_to(
	id: int,
	instance   : String,
	account    : String,
	character  : String,
	unreliable : bool = false
) -> void:
	socket_send_to(id, Packet.Server.player_update, 
		[var_to_str(instance), var_to_str(account), var_to_str(character), id], unreliable)

func send_player_update_to_all(
	id: int,
	instance   : String,
	account    : String,
	character  : String,
	unreliable : bool = false
) -> void:
	socket_send_to_all(Packet.Server.player_update, 
		[var_to_str(instance), var_to_str(account), var_to_str(character), id], unreliable)

func send_player_update_to_map(
	id: int,
	map_index: int,
	instance   : String,
	account    : String,
	character  : String,
	unreliable : bool = false
) -> void:
	socket_send_to_map(map_index, Packet.Server.player_update,
		[var_to_str(instance), var_to_str(account), var_to_str(character), id], unreliable)

func send_player_move(id: int) -> void:
	var player : Player = Session.get_player(id)
	
	# pack player movement data
	var packet : Array = [
		player.character.pos_x,
		player.character.pos_y,
		player.instance.move_speed,
		player.character.direction,
		player.instance.move_type,
		id
	]
	
	socket_send_to_map(player.character.map, Packet.Server.player_move, packet, true)

func send_player_access(id: int) -> void:
	socket_send_to_all(Packet.Server.player_access, 
						[Session.get_player(id).account.access, id], true)

func send_player_sprite(id: int) -> void:
	var player : Player = Session.get_player(id)
	socket_send_to_map(player.character.map, Packet.Server.player_sprite, 
						[player.character.sprite, id], true)

func send_player_equipment(id: int) -> void:
	var player : Player = Session.get_player(id)
	socket_send_to_map(player.character.map, Packet.Server.player_equipment, 
						[var_to_str(player.character.equipment), id], true)

func send_chat(id: int, message: String) -> bool:
	# remove some garbage then determine if theres a message to send
	message = message.strip_edges()
	if message.length() < 1: return false # hacking attempt
	message = BB_Text.trim_spaces(message)
	if message.length() < 1: return false # hacking attempt
	message = Session.authorize_new_lines(id, message)
	if not BB_Text.has_text_excluding_tags(message): return true
	
	# clean up bb_code tags
	message = Session.strip_unauthorized_tags(id, message)
	if message.length() < 1: return true
	message = BB_Text.clean_tags(message)
	
	# get player info
	var player : Player = Session.get_player(id)
	var pname  : String = "[color=%s]" + player.character.name + ": [/color]"
	
	match player.account.access:
		Globals.Access.player:        pname %= Globals.CHAT_COLOR_PLAYER
		Globals.Access.moderator:     pname %= Globals.CHAT_COLOR_MODERATOR
		Globals.Access.developer:     pname %= Globals.CHAT_COLOR_DEVELOPER
		Globals.Access.administrator: pname %= Globals.CHAT_COLOR_ADMINISTRATOR
	
	# say
	message = ("[color=%s]" % Globals.CHAT_COLOR_SAY) + message + "[/color]"
	socket_send_to_map(player.character.map, Packet.Server.chat, [pname + message], true)
	
	return true # successfully received
