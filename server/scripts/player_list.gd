class_name PlayerList
const _categories : Array[String] = [
	"UUID",
	"IP",
	"Username",
	"Character",
	"Role"
]
static var _format : String = "[center]| [color=gray]%-10s | %-15s | %-15s | %-15s | %-10s[/color] |[/center]"
static var _list   : Array[int]

static func _player_to_list(id: int) -> String:
	var player    : Player = Session.get_player(id)
	var username  : String = "..."
	var character : String = "..."
	var role      : String = "..."
	
	# get username
	if player.account != null:
		username = player.account.username
		role     = Globals.Access.keys()[player.account.access]
		
		# get character name (only if account exists)
		if player.instance.status == Player.Status.in_game:
			character = player.character.name
	
	return _format % [
		str(id),               # network id
		Network.socket_ip(id), # ip address
		username,              # username
		character,             # character name
		role
	]

static func add_player(id: int) -> void:
	_list.append(id)
	
static func remove_player(id: int) -> void:
	_list.erase(id)

static func update_player(id: int) -> void:
	pass # _list[0] = _player_to_list(id)

static func display():
	Console.clear.call()
	Console.write.call("\n")
	
	# page info
	var page         : int = 1
	var per_page     : int = 20
	
	var start_index  : int = (page - 1) * per_page
	var end_index    : int = start_index + per_page
	var list_size    : int = _list.size()
	var page_players :     = _list.slice(start_index, min(end_index, list_size))
	
	# print separator
	Console.write.call("[center]|" + "=".repeat(79) + "|[/center]")
	
	# print categories
	Console.write.call(_format % _categories)
	
	# print separator
	Console.write.call("[center]|" + "=".repeat(79) + "|[/center]")
	
	# print players info
	for player in page_players:
		Console.write.call(_player_to_list(player))
	
	# print separator
	Console.write.call("[center]|" + "=".repeat(79) + "|[/center]")
	
	Console.write.call("\n\n" + (
		"[color=green]Page[/color] " +
		"[color=white]%d[/color] " +
		"[color=green]of[/color] " +
		"[color=white]%d[/color]"
	) % [page, ceil(list_size / float(per_page))])
	
	Console.write.call(
		"[[color=yellow]N[/color]] Next page | " + 
		"[[color=yellow]P[/color]] Previous page | " +
		"[[color=yellow]Q[/color]] Quit"
	)


########################################
#### Helper Functions
###############################
#
#func _line_size() -> int: return _categories.size()
#
#func _clear_list() -> void:
	#_player_list.clear()
	#
	## add categories to top
	#for category in _categories:
		#_player_list.add_item(category)
#
#func _index_to_line(index: int) -> int:
	## align to the row index is at
	#var line_size : int = _line_size()
	#return index * line_size
#
#func _player_to_list(id: int) -> Array[String]:
	#var player    : Player = Session.get_player(id)
	#var username  : String
	#var character : String
	#
	## get username
	#if player.account != null:
		#username = player.account.username
		#
		## get character name (only if account exists)
		#if player.instance.status == Player.Status.in_game:
			#character = player.character.name
	#
	#return [
		#str(id),               # network id
		#Network.socket_ip(id), # ip address
		#username,              # username
		#character              # character name
	#]
#
#func _index_to_id(index: int) -> int:
	#index = _index_to_line(index)
	#return _player_list.get_item_text(index).to_int()
#
#func _id_to_index(id: int) -> int:
	#var line_size : int = _line_size()
	#var index     : int
	#
	## find offset and divide by line size
	#for i in range(0, _player_list.item_count, line_size):
		#if _player_list.get_item_text(i).to_int() == id:
			#index = i / line_size
			#break
	#
	#return index
#
########################################
#### Internal accessors
###############################
#
#func _update_player(index: int) -> void:
	#var line_index : int = _index_to_line(index)
	#
	## get network id and player info list
	#var id    : int           = _player_list.get_item_text(line_index).to_int()
	#var items : Array[String] = _player_to_list(id)
	#
	## iterate list and update fields
	#var i : int = 0
	#for item in items:
		#_player_list.set_item_text(line_index + i, item)
		#i += 1
