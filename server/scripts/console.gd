class_name Console

static var clear : Callable = cli_clear
static var write : Callable = cli_write
static var error : Callable = cli_error

static func cli_clear() -> void:
	print("\u001b[;H\u001b[2J\u001b[3J")
	print_rich("[b][color=purple]LORPG[/color] [color=yellow]SERVER[/color][/b]\n")

static func cli_write(line: String) -> void:
	print_rich(line)

static func cli_error(line: String) -> void:
	print_rich("[b][color=red]ERROR:[/color][/b] " + line)
