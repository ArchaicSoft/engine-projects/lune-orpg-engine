class_name Database extends SharedDatabase

#Public MapCache(1 To MAX_MAPS) As Cache
#Public TempTile(1 To MAX_MAPS) As TempTileRec
#Public ResourceCache(1 To MAX_MAPS) As ResourceCacheRec
#Public Bank(1 To MAX_PLAYERS) As BankRec
#Public TempPlayer(1 To MAX_PLAYERS) As TempPlayerRec
#Public Class() As ClassRec
#Public MapItem(1 To MAX_MAPS, 1 To MAX_MAP_ITEMS) As MapItemRec
#Public MapNpc(1 To MAX_MAPS) As MapDataRec
#Public Party(1 To MAX_PARTYS) As PartyRec

#######################################
### Setup Functions
##############################

static func initialize() -> bool:
	Console.write.call("Initializing database started.")
	
	# presizes database arrays
	super._init()
	
	# load database arrays
	if not (
		_load_database(animations, Animation2D) and
		_load_database(classes,    Class)       and
		_load_database(items,      Item)        and
		_load_database(maps,       Map)         and
		_load_database(npcs,       Npc)         and
		_load_database(shops,      Shop)        and
		_load_database(spells,     Spell)       and
		_load_database(supplies,   Supply)
	):
		Utility.log_error("\tFailed to initialize database!")
		return false # there was error loading something
	
	# generate cache to optimize data requests between client/server
	
	
	Console.write.call("\tSuccessfully initialized database.")
	
	return true # there were no errors

static func terminate() -> void: pass

static func save_all() -> void:
	Console.write.call("Saving database.")
	
	_save_database(animations, Animation2D)
	_save_database(classes,    Class)
	_save_database(items,      Item)
	_save_database(maps,       Map)
	_save_database(npcs,       Npc)
	_save_database(shops,      Shop)
	_save_database(spells,     Spell)
	_save_database(supplies,   Supply)
	
	Console.write.call("\tCompleted saving database.")

static func _load_database(db: Array, type: Variant) -> bool:
	var def = type.new()
	var type_name : String = def.type_name()
	
	# make sure we have a array to cache to
	if not Session.database_cache.has(type_name):
		Session.database_cache[type_name] = {}
	
	for i in db.size():
		db[i] = def.load_self(i)
		Session.database_cache[type_name][i] = var_to_str(db[i]) # add to cache
	return true # successfully loaded everything

static func _save_database(db: Array, type: Variant) -> void:
	var def = type.new()
	var type_name : String = def.type_name()
	
	Console.write.call("\t\tSaving %s's." % type_name.capitalize())
	
	for i in db.size():
		db[i].save_self(i)

static func reload_array(db: Array, type: Variant) -> void:
	var def = type.new()
	var type_name : String = def.type_name()
	
	Console.write.call("Reloading %s's." % type_name.capitalize())
	
	_load_database(db, type)
	
	Console.write.call("\tFinished reloading %s's." % type_name.capitalize())
