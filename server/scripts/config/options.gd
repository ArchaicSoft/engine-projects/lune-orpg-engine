class_name OptionsDef extends ConfigResource
func _name() -> String: return "Options"

@export var game_name   : String = "Lune ORPG"
@export var port        : int    = 7001
@export var max_clients : int    = 100
@export var use_ssl     : bool   = false
@export var debugging   : bool   = false

@export var welcome     : String = "Welcome to [color=purple]Lune ORPG Engine[/color]. Enjoy your stay!"
@export var website     : String = "https://archaicsoft.forumotion.com/"
@export var news        : String = "This engine is currently in alpha!"
