class_name Paths

#######################################
### Routing Paths
##############################

static func current_directory() -> String:
	return OS.get_executable_path().get_base_dir() + "/"

static func get_directory(external: bool) -> String:
	return "user://" if external else "res://"

#######################################
### Sub-Routing Paths
##############################

static func config(external: bool = true) -> String:
	return get_directory(not OS.has_feature("standalone") or external) + "config/"

static func assets(external: bool = false) -> String:
	return get_directory(external) + "assets/"

static func database(external: bool = true) -> String:
	return get_directory(not OS.has_feature("standalone") or external) + "database/"

static func logs() -> String:
	return get_directory(true) + "logs/"

#######################################
### Database Paths
##############################

static func animations(external: bool = true) -> String:
	return  database(external) + "animations/"

static func classes(external: bool = true) -> String:
	return  database(external) + "classes/"

static func items(external: bool = true) -> String:
	return  database(external) + "items/"

static func maps(external: bool = true) -> String:
	return  database(external) + "maps/"

static func npcs(external: bool = true) -> String:
	return  database(external) + "npcs/"

static func shops(external: bool = true) -> String:
	return  database(external) + "shops/"

static func spells(external: bool = true) -> String:
	return  database(external) + "spells/"

static func supplies(external: bool = true) -> String:
	return  database(external) + "supplies/"

#######################################
### User Data Paths
##############################

static func accounts() -> String:
	return get_directory(true) + "accounts/"
