extends Node

##
# This is the master Game-Loop
#
# @category function
#
# @params delta : The time passed between render frames
##
func _physics_process(delta: float) -> void:
	# check all the active maps
	for index in Session.map_instances.keys():
		var map_inst : MapInstance = Session.get_map_instance(index)
		var map      : Map         = Database.maps[index]
		
		# check player stuff
		for id in map_inst.players:
			_handle_player(map, id, delta)

func _handle_player(map: Map, id: int, delta: float) -> void:
	var player : Player = Session.get_player(id)
	
	# determine if need to decrement movement timer
	if player.instance.request_move_timer > 0.0:
		player.instance.request_move_timer -= delta
	
	# check if need to process a move request
	if (
		player.instance.request_movement and
		player.instance.request_move_timer <= 0.0
	): # they wants to move
		
		var cur_pos : Vector2 = player.character.get_position()
		
		# check if they can move to tile
		match player.instance.request_move_dir:
			Globals.Direction.up:
				player.character.direction = Globals.Direction.up
				cur_pos.y -= 1
			
			Globals.Direction.left:
				player.character.direction = Globals.Direction.left
				cur_pos.x -= 1
			
			Globals.Direction.right:
				player.character.direction = Globals.Direction.right
				cur_pos.x += 1
			
			Globals.Direction.down:
				player.character.direction = Globals.Direction.down
				cur_pos.y += 1
		
		# handle boundary collisions
		if (
			cur_pos.x < 0 or cur_pos.y < 0 or
			cur_pos.x >= map.size.x or cur_pos.y >= map.size.y
		):
			# warp them to connected map
			if map.connected_map[player.character.direction] > -1:
				pass
			
			# pop them back in bounds
			else:
				cur_pos.x = clampi(cur_pos.x as int, 0, map.size.x - 1) as float
				cur_pos.y = clampi(cur_pos.y as int, 0, map.size.y - 1) as float
		
		# determine if we actually changed tiles
		if (
			cur_pos.x != player.character.pos_x or
			cur_pos.y != player.character.pos_y
		):
			# update position
			player.character.set_position(cur_pos)
			
			# handle tile attributes
			pass
		
		# determine speed
		match player.instance.request_move_type:
			Globals.Movement.standing:
				player.instance.move_speed = 0.0 # No movement
			Globals.Movement.walking:
				player.instance.move_speed = Globals.MOVE_SPEED_WALKING
			Globals.Movement.running:
				player.instance.move_speed = Globals.MOVE_SPEED_RUNNING
		player.instance.move_type = player.instance.request_move_type
		
		# assign timer to move_speed
		player.instance.request_move_timer += player.instance.move_speed
		
		# send a packet to map
		Network.send_player_move(id)
