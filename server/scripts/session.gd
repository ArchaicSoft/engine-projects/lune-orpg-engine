extends Node

# session variables

#######################################
### Live Data Structures
##############################

# configs
var options         := OptionsDef.new()

# cached references
var active_accounts : Array[String]
var taken_names     : Array[String]
var database_cache  : Dictionary # of all game data

# dynamic database
var players         : Dictionary # of Player
var map_instances   : Dictionary # of MapInstance

#######################################
### Setup Functions
##############################

func _ready() -> void:
	# wait for gui if used
	if not DisplayServer.get_name() == "headless":
		await get_tree().process_frame
	
	# present startup text
	Console.clear.call()
	
	# try to load configs first
	options = options.load_self()
	
	# ensure everything initializes properly
	if not (
		Database.initialize() and
		Network.initialize(Session.options.port, Session.options.max_clients)
	):
		Console.error.call("One or more components failed to initialize!")

func terminate() -> void:
	Database.terminate()
	Network.terminate()
	get_tree().quit()

#######################################
### Screen Functions
##############################

func get_screen() -> Node:
	return get_tree().current_scene

#######################################
### Map Instance Functions
##############################

func get_map_instance(index: int) -> MapInstance:
	# return existing map instance
	if map_instances.has(index):
		return map_instances[index]
	
	# create instance, one does not exist
	var map_inst :                = MapInstance.new()
	Session.map_instances[index]  = map_inst
	return map_inst
	

#######################################
### User Functions
##############################

##
# Gets the Player Instance of the specified ID.
#
# Params:
#         id : The ID of the Player.
#
# Example:
#         get_player(1)
##
func get_player(id: int) -> Player:
	return players[id] if players.has(id) else null

##
# Gets the access color provided by the Player Instance of the specified ID.
#
# Params:
#         id : The ID of the Player.
#
# Example:
#         get_access_color(1) -> "white"
##
func get_access_color(id: int) -> String:
	match get_player(id).account.access:
		Globals.Access.player:        return Globals.CHAT_COLOR_PLAYER
		Globals.Access.moderator:     return Globals.CHAT_COLOR_MODERATOR
		Globals.Access.developer:     return Globals.CHAT_COLOR_DEVELOPER
		Globals.Access.administrator: return Globals.CHAT_COLOR_ADMINISTRATOR
		_: return Globals.CHAT_COLOR_PLAYER

##
# Returns the full path to an account file given a Username.
#
# Params:
#         username : The Username of the account.
#
# Example:
#         account_string('testUser123') => user:/database/accounts/testUser123.tres
##
func account_string(username: String) -> String:
	return Paths.accounts() + username.to_lower() + ".tres"

##
# Creates a User Account with a provided Username and Password.
#
# Params:
#         username : The Username for the account.
#         password : The Password for the account.
#
# Example:
#         create_account('testUser1234', 'test1234')
##
func create_account(username: String, password: String) -> bool:
	var path : String = account_string(username)
	
	# make sure a file does not already exist
	if FileAccess.file_exists(path): return false
	
	# generate player definition
	var account     := Account.new()
	account.username = username
	account.password = password
	
	var player       = Player.new()
	player.account   = account
	
	# attempt write account file
	return Utility.save_resource(player, path) == OK

##
# Loads a User Account with a provided Username.
#
# Params:
#         username : The Username for the account.
#
# Example:
#         load_account('testUser1234')
##
func load_account(username: String) -> Player:
	var path   : String = account_string(username)
	
	# ensure a file can be accessed
	if not FileAccess.file_exists(path): return null
	
	# attempt read account file
	return Utility.load_resource(path, "Player", ResourceLoader.CACHE_MODE_IGNORE)

##
# Saves a User Account of a provided ID.
#
# Params:
#         id : The ID for the account.
#
# Example:
#         save_account('testUser1234')
##
func save_account(id: int) -> void:
	var player : Player = get_player(id)
	var path   : String = account_string(player.account.username)
	
	# make sure they have data to write
	if player.account == null: return
	
	# attempt write account file
	var err = Utility.save_resource(player, path)
	if err != OK:
		Console.write.call("Error saving user account [" + player.account.username + "]:")
		Console.write.call("\tCode (%d)" % err)

#######################################
### Account Screen Functions
##############################

func process_login_successful(id: int, tmp_player: Player) -> void:
	# send them game data
	Network.send_game_data(id)
	
	# successful login
	active_accounts.append(tmp_player.account.username)
	
	# assign their data and send them off
	var player : Player = get_player(id)
	player.account      = tmp_player.account
	player.character    = tmp_player.character
	
	# change their game state
	player.instance.status = Player.Status.logged_in
	
	Network.send_enter_account(id)

#######################################
### Chat Functions
##############################

##
# Finds and returns the a String with any Unauthorized BBCode Tags removed. This can be due to privelage.
#
# Params:
#         id      : The ID of the Player sending the message.
#         message : The Message to search for unauthorized tags.
#
# Returns:
#         A String in which has been trimmed of Unauthorized BBCode Tags.
#
# Example:
#         strip_unauthorized_tags(1, '[url]www.google.ca[/url]') => www.google.ca
##
func strip_unauthorized_tags(id: int, message: String) -> String:
	match get_player(id).account.access:
		Globals.Access.player:        return BB_Text.strip_tags(message, ["url", "img", "table", "cell"])
		Globals.Access.moderator:     return BB_Text.strip_tags(message, ["url", "img", "table", "cell"])
		Globals.Access.developer:     return message
		Globals.Access.administrator: return message
	return message

##
# Determines if a message sent by a specific player is authorized to contain new lines breaks.
#
# Params:
#         id      : The ID of the Player sending the message.
#         message : The Message to search for unauthorized tags.
# Returns:
#         A String in which has been trimmed of Unauthorized Line Breaks.
#
# Example:
#         authorize_new_lines(1, 'www.google.ca /n www.google.ca') => www.google.ca www.google.ca
##
func authorize_new_lines(id: int, message: String) -> String:
	match get_player(id).account.access:
		Globals.Access.player:        return BB_Text.strip_new_lines(message)
		Globals.Access.moderator:     return BB_Text.strip_new_lines(message)
		Globals.Access.developer:     return message # BB_Text.insert_new_lines(message) # todo: Fix this?
		Globals.Access.administrator: return message # BB_Text.insert_new_lines(message) # todo: Fix this?
	return message

func create_chat_message(message: String, color: String = "white") -> String:
	# remove some garbage then determine if theres a message to send
	message = message.strip_escapes()
	if message.length() < 1: return ""
	message = BB_Text.trim_spaces(message)
	if message.length() < 1: return ""
	# message = BB_Text.insert_new_lines(message) # todo: Fix this?
	if not BB_Text.has_text_excluding_tags(message): return ""
	
	# clean up bb_code tags
	message = BB_Text.clean_tags(message)
	
	# wrap message in selected color
	return "[color=%s]%s[/color]" % [color, message]
