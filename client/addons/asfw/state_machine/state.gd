class_name State
func get_class() -> String: return "State"
static func get_instance_base_type() -> StringName: return "State"

# state function pointers
var enter           : Callable = func(prev_state: String) -> bool: return true
var process_input   : Callable = func(delta: float)       -> void: pass
var process_physics : Callable = func(delta: float)       -> void: pass
var process         : Callable = func(delta: float)       -> void: pass
var exit            : Callable = func(next_state: String) -> bool: return true

func _init(callables: Dictionary) -> void:
	if callables.is_empty(): return
	
	if callables.has("enter"):
		var callable = callables["enter"]
		if callable is Callable: enter = callable
	
	if callables.has("process_input"):
		var callable = callables["process_input"]
		if callable is Callable: process_input = callable
	
	if callables.has("process_physics"):
		var callable = callables["process_physics"]
		if callable is Callable: process_physics = callable
	
	if callables.has("process"):
		var callable = callables["process"]
		if callable is Callable: process = callable
		
	if callables.has("exit"):
		var callable = callables["exit"]
		if callable is Callable: exit = callable
