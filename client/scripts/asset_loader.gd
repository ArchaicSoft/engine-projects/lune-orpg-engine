class_name AssetLoader

static func popup_error(asset: String) -> Variant:
	var message : String = "Asset[ %s ] could not be found!" % asset
	Overlay.message_box(message, "Missing asset exception")
	return null

static func display_error(asset: String) -> Variant:
	# display messagebox if not in game
	var chatbox = ChatBox.instance()
	if chatbox == null: return popup_error(asset)
	
	var message : String  = "Asset[ %s ] could not be found!" % asset
	ChatBox.instance().chat_text.text += message
	return null

static func _try_load_texture(path: String, resource_loader: bool = false) -> ImageTexture:
	if not FileAccess.file_exists(path): return null
	
	# use resource-loader
	if resource_loader: return load(path)
	
	return ImageTexture.create_from_image(
		Image.load_from_file(path)
	)

static func _try_load_audio(path: String, resource_loader: bool = false) -> Variant:
	if not FileAccess.file_exists(path): return null
	
	# use resource-loader
	if resource_loader: return load(path)
	
	# ensure a file can be accessed
	var file := FileAccess.open(path, FileAccess.READ)
	if not file: return null
	
	# attempt read audio file contents
	var byte_data : PackedByteArray = file.get_buffer(file.get_length())
	var audio : Variant
	
	# assign audio by file type
	match path.get_extension():
		"wav": audio = AudioStreamWAV.new()
		"ogg": audio = AudioStreamOggVorbis.new()
		"mp3": audio = AudioStreamMP3.new()
		_: return null # not a valid type
	audio.data = byte_data
	
	return audio

# audio files

static func load_music(file: String, ignore_error: bool = false) -> Variant:
	# attempt external override first
	var audio : Variant = _try_load_audio(Paths.audio_music(true) + file)
	if audio !=  null: return audio
	
	# attempt internal load next
	audio = _try_load_audio(Paths.audio_music(false) + file, true)
	if audio != null: return audio
	
	# return missing file
	return null if ignore_error else display_error("Music {%s}" % file)

static func load_sound(file: String, ignore_error: bool = false) -> Variant:
	# attempt external override first
	var audio : Variant = _try_load_audio(Paths.audio_sound(true) + file)
	if audio !=  null: return audio
	
	# attempt internal load next
	audio = _try_load_audio(Paths.audio_sound(false) + file, true)
	if audio != null: return audio
	
	# return missing file
	return null if ignore_error else display_error("Sound {%s}" % file)

# graphics paths

static func load_animation(index: int, load_placeholder: bool = true) -> Variant:
	if index < 0: return null
	
	# attempt external override first
	var texture : Variant = _try_load_texture(Paths.graphics_animations(index, true))
	if texture != null: return texture
	
	# attempt internal load next
	texture = _try_load_texture(Paths.graphics_animations(index, false), true)
	if texture != null: return texture
	
	# return a missing placeholder
	return null if not load_placeholder else load_system("missing/icon_image")

static func load_face(index: int, load_placeholder: bool = true) -> Variant:
	if index < 0: return null
	
	# attempt external override first
	var texture : Variant = _try_load_texture(Paths.graphics_faces(index, true))
	if texture != null: return texture
	
	# attempt internal load next
	texture = _try_load_texture(Paths.graphics_faces(index, false), true)
	if texture != null: return texture
	
	# return a missing placeholder
	return null if not load_placeholder else load_system("missing/icon_image")

static func load_gui(file: String) -> Variant:
	file += ".png"
	
	# attempt external override first
	var texture : Variant = _try_load_texture(Paths.graphics_gui(true) + file)
	if texture != null: return texture
	
	# attempt internal load next
	texture = _try_load_texture(Paths.graphics_gui(false) + file, true)
	if texture != null: return texture
	
	# return missing file
	return display_error("Gui {%s}" % file)

static func load_item(index: int, load_placeholder: bool = true) -> Variant:
	if index < 0: return null
	
	# attempt external override first
	var texture : Variant = _try_load_texture(Paths.graphics_items(index, true))
	if texture != null: return texture
	
	# attempt internal load next
	texture = _try_load_texture(Paths.graphics_items(index, false), true)
	if texture != null: return texture
	
	# return a missing placeholder
	return null if not load_placeholder else load_system("missing/icon_image")

static func load_npc(index: int, load_placeholder: bool = true) -> Variant:
	if index < 0: return null
	
	# attempt external override first
	var texture : Variant = _try_load_texture(Paths.graphics_npcs(index, true))
	if texture != null: return texture
	
	# attempt internal load next
	texture = _try_load_texture(Paths.graphics_npcs(index, false), true)
	if texture != null: return texture
	
	# return a missing placeholder
	return null if not load_placeholder else load_system("missing/sprite")

static func load_paperdoll(index: int, load_placeholder: bool = true) -> Variant:
	if index < 0: return null
	
	# attempt external override first
	var texture : Variant = _try_load_texture(Paths.graphics_paperdolls(index, true))
	if texture != null: return texture
	
	# attempt internal load next
	texture = _try_load_texture(Paths.graphics_paperdolls(index, false), true)
	if texture != null: return texture
	
	# return a missing placeholder
	return null if not load_placeholder else load_system("missing/sprite")

static func load_player(index: int, load_placeholder: bool = true) -> Variant:
	if index < 0: return null
	
	# attempt external override first
	var texture : Variant = _try_load_texture(Paths.graphics_players(index, true))
	if texture != null: return texture
	
	# attempt internal load next
	texture = _try_load_texture(Paths.graphics_players(index, false), true)
	if texture != null: return texture
	
	# return a missing placeholder
	return null if not load_placeholder else load_system("missing/sprite")

static func load_supply(index: int, load_placeholder: bool = true) -> Variant:
	if index < 0: return null
	
	# attempt external override first
	var texture : Variant = _try_load_texture(Paths.graphics_supplies(index, true))
	if texture != null: return texture
	
	# attempt internal load next
	texture = _try_load_texture(Paths.graphics_supplies(index, false), true)
	if texture != null: return texture
	
	# return a missing placeholder
	return null if not load_placeholder else load_system("missing/icon_image")

static func load_spell(index: int, load_placeholder: bool = true) -> Variant:
	if index < 0: return null
	
	# attempt external override first
	var texture : Variant = _try_load_texture(Paths.graphics_spells(index, true))
	if texture != null: return texture
	
	# attempt internal load next
	texture = _try_load_texture(Paths.graphics_spells(index, false), true)
	if texture != null: return texture
	
	# return a missing placeholder
	return null if not load_placeholder else load_system("missing/icon_image")

static func load_system(file: String) -> Variant:
	file += ".png"
	
	# attempt external override first
	var texture : Variant = _try_load_texture(Paths.graphics_system(true) + file)
	if texture != null: return texture
	
	# attempt internal load next
	texture = _try_load_texture(Paths.graphics_system(false) + file, true)
	if texture != null: return texture
	
	# return missing file
	return display_error("System {%s}" % file)

static func load_tileset(index: int, load_placeholder: bool = true) -> Variant:
	if index < 0: return null
	
	# attempt external override first
	var texture : Variant = _try_load_texture(Paths.graphics_tilesets(index, true))
	if texture != null: return texture
	
	# attempt internal load next
	texture = _try_load_texture(Paths.graphics_tilesets(index, false), true)
	if texture != null: return texture
	
	# return a missing placeholder
	return null if not load_placeholder else load_system("missing/tile")
