extends RdpClient

##
# Provides access to log debug information in both backend and frontend.
##
func log_debug(message: String) -> void:
	if not Session.options.debugging: return
	receive_alert(message, "Debug info")

##
# Provides access to log information in both backend and frontend.
##
func log_status(message: String) -> void:
	if not Session.options.debugging: return
	Session.log_activity(message)

##
# User-Defined event for interacting on a successful connection attempt.
##
func _connection_succeeded() -> void: pass

##
# User-Defined event for interacting on a failed connection attempt.
##
func _connection_failed() -> void: pass

##
# User-Defined event for interacting on a connection termination.
##
func _connection_ended() -> void:
	# return to menu
	Session.set_screen("Menu")
	
	# check if need to notify we were disconnected
	if Session._my_id != 0:
		Overlay.message_box("Server was disconnected!")
	
	# delete session data related to net id's
	Session._my_id = 0
	Session.players.clear()

##
# Connect or reconnect to the server.
##
func connect_to_server(ip: String = "", port: int = 0) -> void:
	terminate()
	
	var ssl_cert : String
	var ssl_host  : String
	
	if Session.options.use_ssl:
		ssl_cert = "res://assets/resources/security/cert.crt"
		ssl_host = "host_name_here" # todo: modify this later
	
	# select best fit for input
	if ip == "":  ip   = Session.options.ip
	if port == 0: port = Session.options.port
	
	initialize(ip, port, 0, 0, ssl_cert, ssl_host, false)

#######################################
### Receive Functions
##############################

func receive_alert(message: String, title: String = "") -> bool:
	Overlay.clear_status(true)
	Overlay.message_box(message, title)
	return true # successfully received

func receive_game_data(database: Dictionary) -> bool:
	Session.database_cache = database
	
	Database.reload_array(Database.animations)
	Database.reload_array(Database.classes)
	Database.reload_array(Database.items)
	Database.reload_array(Database.maps)
	Database.reload_array(Database.npcs)
	Database.reload_array(Database.shops)
	Database.reload_array(Database.spells)
	Database.reload_array(Database.supplies)
	
	return true # successfully received

func receive_update_data(
	type     : Globals.Editor,
	index    : int,
	data_raw : String
) -> bool:
	var data : IndexedResource
	
	match type:
		Globals.Editor.animation:
			data                       = str_to_var(data_raw)
			Database.animations[index] = data
		
		Globals.Editor.item:
			data                       = str_to_var(data_raw)
			Database.items[index]      = data
		
		Globals.Editor.map:
			data                       = str_to_var(data_raw)
			Database.maps[index]       = data
			Session.refresh_map()
		
		Globals.Editor.npc:
			data                       = str_to_var(data_raw)
			Database.npcs[index]       = data
		
		Globals.Editor.shop:
			data                       = str_to_var(data_raw)
			Database.shops[index]      = data
		
		Globals.Editor.spell:
			data                       = str_to_var(data_raw)
			Database.spells[index]     = data
		
		Globals.Editor.supply:
			data                       = str_to_var(data_raw)
			Database.supplies[index]   = data
	
	# save and cache the updates
	data.save_self(index)
	Session.database_cache[data.type_name()][index] = data_raw
	
	# refresh editor if we are in one
	var editor_screen : EditorScreen = EditorScreen.instance()
	if editor_screen != null: editor_screen.refresh_index_list()
	
	return true # successfully received

func receive_enter_account(player_raw: String) -> bool:
	# extract player data
	var player : Player = str_to_var(player_raw)
	
	# get our network id
	var id : int = player.instance.network_id
	
	# add ourselves to player list
	Session._my_id      = id
	Session.players[id] = player
	
	# send to character select screen
	Session.set_screen("Account")
	var screen : AccountScreen = AccountScreen.instance()
	screen.set_visible_panel(screen.character_select)
	
	return true # successfully received

func receive_enter_game() -> bool:
	Session.set_screen("Game")
	
	return true # successfully received

func receive_leave_game(id: int) -> bool:
	var player : Player = Session.get_player(id)
	
	# check if nothing to do
	if player == null: return true
	
	# remove player object
	Session.players.erase(id)
	
	return true # successfully received

func receive_change_map() -> bool:
	# black out game screen
	
	# reset the world/map
	Session.reset_world()
	
	return true # successfully received

func receive_present_map() -> bool:
	# un-black out game screen
	
	return true # successfully received

func receive_player_join_map(player_raw: String, id: int) -> bool:
	# extract player data
	var player : Player = str_to_var(player_raw)
	
	# assign their entry
	Session.players[id] = player
	
	# spawn a game object
	Session.spawn_player(id)
	
	return true # successfully received

func receive_player_left_map(id: int) -> bool:
	var player : Player = Session.get_player(id)
	
	# set their map to none so their game object suicides
	player.character.map = -1
	
	return true # successfully received

func receive_player_update(
	instance  : String,
	account   : String,
	character : String,
	id        : int
) -> bool:
	var player : Player = Session.get_player(id)
	
	# update player info
	if instance  != null and instance.length()  > 1: player.instance  = str_to_var(instance)
	if account   != null and account.length()   > 1: player.account   = str_to_var(account)
	if character != null and character.length() > 1: player.character = str_to_var(character)
	
	return true # successfully received

func receive_player_move(
	pos_x          : float,
	pos_y          : float,
	movement_speed : float,
	direction_type : Globals.Direction,
	movement_type  : Globals.Movement,
	id    : int
) -> bool:
	var player : Player = Session.get_player(id)
	
	# update player info
	player.character.pos_x     = pos_x
	player.character.pos_y     = pos_y
	player.instance.move_speed = movement_speed
	player.character.direction = direction_type
	player.instance.move_type  = movement_type
	
	return true # successfully received

func receive_player_access(access: Globals.Access, id: int) -> bool:
	Session.get_player(id).account.access = access
	return true # successfully received

func receive_player_sprite(sprite: int, id: int) -> bool:
	Session.get_player(id).character.sprite = sprite
	return true # successfully received

func receive_player_equipment(equipment: Array[ItemSlot], id: int) -> bool:
	Session.get_player(id).character.equipment = equipment
	return true # successfully received

func receive_chat(message: String) -> bool:
	if GameScreen.instance() == null: return true # successfully received
	
	var box : ChatBox = ChatBox.instance()
	
	# only add new line on existing chat session
	if box.chat_text.text.length() > 0:
		message = "\n" + message
	
	# append chat data
	box.chat_text.text += message
	
	return true # successfully received

#######################################
### Send Functions
##############################

func send_login(username: String, password: String) -> void:
	if not socket_connected():
		Overlay.set_status("Server is not currently available", 
			true, true, 5.0, func(): Session.set_screen("Menu"))
		return
	
	Overlay.set_status(
		"Sending login information", true, true, 30.0,
		func(): Overlay.set_status("Server failed to respond", true, false, 5.0)
	)
	
	socket_send(Packet.Client.login, [username, password])

func send_register(username: String, password: String) -> void:
	if not socket_connected():
		Overlay.set_status("Server is not currently available", 
			true, true, 5.0, func(): Session.set_screen("Menu"))
		return
	
	Overlay.set_status(
		"Sending registration information", true, true, 30.0,
		func(): Overlay.set_status("Server failed to respond", true, false, 5.0)
	)
	
	socket_send(Packet.Client.register, [username, password])

func send_character_create(
	char_name    : String,
	sex          : Globals.Sex,
	class_type   : int,
	sprite_index : int
) -> void:
	if not socket_connected():
		Overlay.set_status("Server is not currently available", 
			true, true, 5.0, func(): Session.set_screen("Menu"))
	
	Overlay.set_status(
		"Sending login information", true, true, 30.0,
		func(): Overlay.set_status("Server failed to respond", true, false, 5.0)
	)
	
	socket_send(Packet.Client.character_create, [char_name, sex, class_type, sprite_index])

func send_character_select() -> void:
	socket_send(Packet.Client.character_select)

func send_character_delete() -> void:
	socket_send(Packet.Client.character_delete)

func send_chat(message: String) -> void:
	socket_send(Packet.Client.chat, [message], true)

func send_request_move(
	request_movement  : bool,
	request_move_dir  : Globals.Direction,
	request_move_type : Globals.Movement
) -> void:
	socket_send(
		Packet.Client.request_move, [
			request_movement,
			request_move_dir,
			request_move_type
		]
	)

func send_editor_opened() -> void:
	socket_send(Packet.Client.editor_opened)

func send_editor_closed() -> void:
	socket_send(Packet.Client.editor_closed)

func send_editor_saved(
	type     : Globals.Editor,
	index    : int,
	data_raw : String
) -> void:
	Overlay.set_status(
		"Sending data to server... awaiting response",
		true,  true, 30, func():
			Overlay.message_box("Server has not responded yet. It may respond soon, or is experiencing network issues!")
	)
	socket_send(Packet.Client.editor_saved, [type, index, data_raw])
