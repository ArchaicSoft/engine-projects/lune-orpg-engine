extends StateMachine

var axis: Vector2 # example control axis storage
var jump: bool    # example jump detection

# in ready you setup the available states
func _ready() -> void:
	
	# example state. may contain all these function fields, though omitting them is valid
	# write_state("Example", State.new({
	# 	"enter":           func(prev_state: String):   return true, # must return true to succeed change
	# 	"process_input":   func(delta: float):         pass,
	# 	"process_physics": func(delta: float):         pass,
	# 	"process":         func(delta: float):         pass,
	# 	"exit":            func(next_state: String):   return true  # must return true to succeed change
	# }))
	
	
	# this can be done by the way the example showcases, or you can
	# use an example like below where write_state is called instead
	# and just call set_default("Idle") afterwards. A state value
	# is only overwritten with this function if one is provided
	set_default("Idle", State.new({
		"process_input": func(delta: float):
			jump = Input.is_action_just_pressed("ui_up")
			
			axis = Vector2(
				Input.get_axis("ui_left", "ui_right"),
				Input.get_axis("ui_up", "ui_down"),
			)\
		,
	
		"process_physics": func(delta: float):
			if   jump:            goto("Jump")
			elif axis.y > 0:      goto("Crouch")
			elif axis.x != 0:     goto("Walk")
			else:                 return        # do nothing
	}))
	
	# this will add the state. will fail if "Walk" was already defined though
	add_state("Walk", State.new({
		"process_physics": func(delta: float):
			var dir : String
			if axis.x < 0: dir = "left."
			else: dir = "right."
			print("I Walked " + dir)
			goto("Idle")
	}))
	
	# this will overwrite the state. succeeds even if "Crouch" wasnt already defined
	write_state("Crouch", State.new({
		"process_physics": func(delta: float):
			print("I am crouching.")
			goto("Idle")
	}))
	
	# write_state is the preferred version to use unless you have specific use case
	# that should only add new states if they dont exist, rather than override
	write_state("Jump", State.new({
		"process_physics": func(delta: float):
			print("I jumped.")
			goto("Idle")
	}))
	
	# sets our game object to the default state
	goto_default()
	
	# could optionally just do this
	# goto("Idle")
