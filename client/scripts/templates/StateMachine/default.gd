extends StateMachine

# in ready you setup the available states
func _ready() -> void:
	
	# example state. may contain all these function fields, though omitting them is valid
	set_default("Example", State.new({
		"enter":           func(prev_state: String):   return true, # must return true to succeed change
		"process_input":   func(delta: float):         pass,
		"process_physics": func(delta: float):         pass,
		"process":         func(delta: float):         pass,
		"exit":            func(next_state: String):   return true  # must return true to succeed change
	}))
	
	# sets our game object to the default
	goto_default()
