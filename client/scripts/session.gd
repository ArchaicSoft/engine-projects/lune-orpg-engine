extends Node

# session variables

var _my_id        : int

#######################################
### Live Data Structures
##############################

# configs
var options        := OptionsDef.new()

# cached references
var database_cache : Dictionary # of all game data
var world_instance : WorldObj

# dynamic database
var players        : Dictionary # of PlayerDef

#######################################
### Setup Functions
##############################

func _ready() -> void:
	# try to load configs first
	options = options.load_self()
	
	# ensure everything initializes properly
	if not (
		Database.initialize() # and
		# Network.initialize()  <- menu will reconnect!
	):
		OS.alert("One or more components failed to initialize!", "Initialization failed")
		terminate()
	
	# set audio player volumes
	Audio.set_music_volume(Session.options.volume_music)
	Audio.set_sound_volume(Session.options.volume_sound)

func terminate() -> void:
	Network.terminate()
	get_tree().quit()

#######################################
### Log Functions
##############################

func _write_log(message: String, path: String, concat_today: bool) -> void:
	# adjust path
	path = Paths.logs() + path
	
	# ensure directory exists
	Utility.check_path(path)
	
	# incorporate time
	if concat_today:
		path    = Time.get_date_string_from_system()
		message = Time.get_time_string_from_system() + ": " + message
	else:
		path    = Time.get_datetime_string_from_system()
	
	# perform io
	var file := FileAccess.open(path, FileAccess.READ_WRITE)
	
	if not file: # file not accessible
		Overlay.message_box("Logging file failed, attempt blocked!")
		return
	
	file.seek_end()
	file.store_line(message)
	file.close()

##
# Creates a Log Message that will be wrote to a File within the Activity Logs folder.
# Activity Logs are non error events that you want documented for historical record keeping.
# This Function has the option to Add the Current Time to the Log Messsage, or not.
# This Log File will save with the File Name of the Current Date.
#
# @category Function
#
# @param message      : The Message to log.
# @param concat_today : Append Current Time to the log message or just Current Date to the File Name.
#
# @example:
# log_activity('Patch [id] was successfully applied.', true)
##
func log_activity(message: String, concat_today: bool = true) -> void:
	_write_log(message, "activity/", concat_today)

##
# Creates a Log Message that will be wrote to a File within the Error Logs folder.
# This Function has the option to Add the Current Time to the Log Messsage, or not.
# This Log File will save with the File Name of the Current Date.
#
# @category Function
#
# @param message      : The Message to log.
# @param concat_today : Append Current Time to the log message or just Current Date to the File Name.
#
# @example:
# log_error('Player object was null!', true)
##
func log_error(message: String, concat_today: bool = true) -> void:
	_write_log(message, "errors/", concat_today)

#######################################
### Screen Functions
##############################

##
# Returns current screen even to classes
#
# @category Function
##
func get_scene() -> Node: return get_tree().current_scene

##
# Returns if the current screen objects name matches the one provided
#
# @category Function
#
# @param screen_name : Name of the screen to load.
#
# @example:
# is_screen("Menu"): { MenuScreen } => true
##
func is_screen(screen_name: String) -> bool:
	return str(get_tree().current_scene.get_child(-1).name) == screen_name.to_lower()

##
# Sets the Screen Managers current Scene to a specified scene.
#
# @category Function
#
# @param screen_name : Name of the screen to load.
#
# @example:
# set_screen("Menu")
##
func set_screen(screen_name: String) -> void:
	var scene      : Node = get_tree().current_scene
	var has_screen : bool = false
	
	# determine if we have this screen already
	for child in scene.get_children():
		var _name = child.name
		if str(child.name).to_lower() == screen_name.to_lower():
			has_screen = true
			break
	
	# only change if screen is different
	if has_screen:
		# just clear any status and leave
		Overlay.clear_status(false)
		return
	
	# schedule previous screen for deletion
	if scene.get_child_count() > 1:
		var old_screen : Control = scene.get_child(1)
		old_screen.hide()
		old_screen.queue_free()
	
	# add the screen
	var screen = load("res://scenes/screens/%s.tscn" % screen_name)
	scene.add_child(screen.instantiate())
	
	# clear display status'
	Overlay.clear_status(true)

#######################################
### Chat Functions
##############################

func parse_command(message: String) -> void:
	var command_len : int    = message.strip_edges().length()
	var command     : String = message.substr(1, command_len)
	
	match command:
		"/map": GameScreen.instance().open_editor("Map")
	
	print("'" + command + "'")

#######################################
### User Functions
##############################

##
# Gets the Player Instance of the specified ID.
#
# @category Function
#
# @param id : The ID of the Player.
#
# @example:
# get_player(1)
##
func get_player(id: int) -> Player:
	return players[id] if players.has(id) else null

func my_player() -> Player:
	return players[_my_id] if _my_id != 0 else null

func player_is_me(id: int) -> bool:
	return id == _my_id

func is_access_at_least(access: Globals.Access) -> bool:
	return my_player().account.access >= access

##
# Gets the access color provided by the Player Instance of the specified ID.
#
# @category Function
#
# @param id : The ID of the Player.
#
# @example:
# get_access_color(1) -> "white"
##
func get_access_color(id: int) -> String:
	match get_player(id).account.access:
		Globals.Access.player:        return Globals.CHAT_COLOR_PLAYER
		Globals.Access.moderator:     return Globals.CHAT_COLOR_MODERATOR
		Globals.Access.developer:     return Globals.CHAT_COLOR_DEVELOPER
		Globals.Access.administrator: return Globals.CHAT_COLOR_ADMINISTRATOR
		_: return Globals.CHAT_COLOR_PLAYER

func get_current_map() -> Map:
	var player = my_player()
	return null if (
		player           == null or
		player.character == null
	) else Database.maps[0] if Database.maps.size() < 1 else \
				Database.maps[player.character.map]

#######################################
### World Functions
##############################

# get functions

func get_world() -> WorldObj:
	if world_instance == null:
		world_instance = WorldObj.instance()
	
	return world_instance

func get_world_player(id: int) -> PlayerObj:
	var world  : WorldObj  = get_world()
	var player : PlayerObj = world.players.find_child(str(id))
	return player if player != null and player.is_queued_for_deletion() else null

# reset functions

# disables and free all children nodes
func _reset_category(world: WorldObj, node_name: String) -> void:
	for child in world.get_node(node_name).get_children():
		child.name = "_" + str(child.name)
		child.set_process(false)
		child.set_physics_process(false)
		child.visible = false
		child.queue_free()

func reset_world() -> void:
	var world : WorldObj = get_world()
	
	_reset_category(world, "Players")
	_reset_category(world, "Npcs")

# spawn functions

func spawn_object(path: String, new_name: String = "") -> Node:
	var packed_scene = load("res://scenes/gameplay/%s.tscn" % path.capitalize())
	if packed_scene == null:
		Overlay.message_box("Failed to spawn object [%s]" % path)
		return null
	
	var object : Node = packed_scene.instantiate()
	if new_name.length() > 0: object.name = new_name
	return object

func spawn_player(id: int) -> void:
	if get_world_player(id) != null: return
	# if we didn't crash world_instance != null
	
	# add player to the world
	world_instance.players.add_child(
		spawn_object("Player", str(id))
	)

func refresh_map() -> void:
	var map : MapObj = WorldObj.instance().map
	map.clear()
	map.draw_map(get_current_map())
