class_name OptionsDef extends ConfigResource
func _name() -> String: return "Options"

@export var game_name    : String = "Lune ORPG"
@export var ip           : String = "127.0.0.1"
@export var port         : int    = 7001
@export var use_ssl      : bool   = false
@export var debugging    : bool   = false

@export var username     : String = ""
@export var password     : String = ""
@export var save_user    : bool   = false
@export var save_pass    : bool   = false

@export var volume_music : float  = 0.3
@export var volume_sound : float  = 0.5
@export var menu_music   : String = ""
@export var button_down  : String = "Click.wav"
@export var button_hover : String = "Button Hover.wav"
