class_name MovementQueueInst

var move_dir   : Globals.Direction
var move_type  : Globals.Movement
var move_speed : float
var position   : Vector2

func _init(
	new_direction : Globals.Direction = Globals.Direction.down,
	new_type      : Globals.Movement  = Globals.Movement.standing,
	new_speed     : float                 = 0.0,
	new_position  : Vector2               = Vector2(-1000, -1000)
) -> void:
	self.move_dir   = new_direction
	self.move_speed = new_speed
	self.move_type  = new_type
	self.position   = new_position
