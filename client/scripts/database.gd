class_name Database extends SharedDatabase

#Public Bank As BankRec
#Public TempTile() As TempTileRec
#Public Class() As ClassRec
#Public MapItem(1 To MAX_MAP_ITEMS) As MapItemRec
#Public MapNpc(1 To MAX_MAP_NPCS) As MapNpcRec
#
#' client-side stuff
#Public ActionMsg(1 To MAX_BYTE) As ActionMsgRec
#Public Blood(1 To MAX_BYTE) As BloodRec
#Public AnimInstance(1 To MAX_BYTE) As AnimInstanceRec
#Public MenuButton(1 To MAX_MENUBUTTONS) As ButtonRec
#Public MainButton(1 To MAX_MAINBUTTONS) As ButtonRec
#Public Party As PartyRec

#######################################
### Setup Functions
##############################

static func initialize() -> bool:
	# presizes database arrays
	super._init()
	
	# load database arrays
	if not (
		_load_database(animations, Animation2D) and
		_load_database(classes,    Class)       and
		_load_database(items,      Item)        and
		_load_database(maps,       Map)         and
		_load_database(npcs,       Npc)         and
		_load_database(shops,      Shop)        and
		_load_database(spells,     Spell)       and
		_load_database(supplies,   Supply)
	):
		Overlay.message_box("Failed to initialize database!")
		return false # there was error loading something
	
	return true # there were no errors

static func terminate() -> void: pass

static func _load_database(db: Array, type: Variant) -> bool:
	var def = type.new()
	var type_name : String = def.type_name()
	
	# make sure we have a array to cache to
	if not Session.database_cache.has(type_name):
		Session.database_cache[type_name] = {}
	
	for i in db.size():
		db[i] = def.load_self(i)
		Session.database_cache[type_name][i] = var_to_str(db[i]) # add to cache
	
	return true # successfully loaded everything

static func reload_array(db: Array) -> void:
	# ensure there is something to reload
	if db.size() < 1: return
	
	var type_name : String     = db[0].type_name()
	var cache     : Dictionary = Session.database_cache
	
	# pull all instances from dictionary
	for i in db.size():
		db[i] = str_to_var(cache[type_name][i])
