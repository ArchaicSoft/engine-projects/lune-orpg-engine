class_name Paths

#######################################
### Routing Paths
##############################

static func current_directory() -> String:
	return OS.get_executable_path().get_base_dir() + "/"

static func get_directory(external: bool) -> String:
	return "user://" if external else "res://"

#######################################
### Sub-Routing Paths
##############################

static func config(external: bool = true) -> String:
	return get_directory(OS.has_feature("standalone") or external) + "config/"

static func assets(external: bool = false) -> String:
	return get_directory(external) + "assets/"

static func database(external: bool = true) -> String:
	return get_directory(not OS.has_feature("standalone") or external) + "database/"

static func logs() -> String:
	return get_directory(true) + "logs/"

#######################################
### Asset Paths
##############################

# audio paths

static func audio_music(external: bool = false) -> String:
	return assets(external) + "audio/music/"

static func audio_sound(external: bool = false) -> String:
	return assets(external) + "audio/sound/"

# graphics paths

static func graphics_animations(index: int, external: bool = false) -> String:
	return assets(external) + "images/animations/" + str(index) + ".png"

static func graphics_faces(index: int, external: bool = false) -> String:
	return assets(external) + "images/faces/" + str(index) + ".png"

static func graphics_gui(external: bool = false) -> String:
	return assets(external) + "images/gui/"

static func graphics_items(index: int, external: bool = false) -> String:
	return assets(external) + "images/items/" + str(index) + ".png"

static func graphics_npcs(index: int, external: bool = false) -> String:
	return assets(external) + "images/npcs/" + str(index) + ".png"

static func graphics_paperdolls(index: int, external: bool = false) -> String:
	return assets(external) + "images/paperdolls/" + str(index) + ".png"

static func graphics_players(index: int, external: bool = false) -> String:
	return assets(external) + "images/players/" + str(index) + ".png"

static func graphics_spells(index: int, external: bool = false) -> String:
	return assets(external) + "images/spells/" + str(index) + ".png"

static func graphics_supplies(index: int, external: bool = false) -> String:
	return assets(external) + "images/supplies/" + str(index) + ".png"

static func graphics_system(external: bool = false) -> String:
	return assets(external) + "images/system/"

static func graphics_tilesets(index: int, external: bool = false) -> String:
	return assets(external) + "images/tilesets/" + str(index) + ".png"

#######################################
### Database Paths
##############################

static func animations(external: bool = false) -> String:
	return  database(external) + "animations/"

static func classes(external: bool = false) -> String:
	return  database(external) + "classes/"

static func items(external: bool = false) -> String:
	return  database(external) + "items/"

static func maps(external: bool = false) -> String:
	return  database(external) + "maps/"

static func npcs(external: bool = false) -> String:
	return  database(external) + "npcs/"

static func shops(external: bool = false) -> String:
	return  database(external) + "shops/"

static func spells(external: bool = false) -> String:
	return  database(external) + "spells/"

static func supplies(external: bool = false) -> String:
	return  database(external) + "supplies/"
