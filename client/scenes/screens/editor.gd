class_name EditorScreen extends ColorRect
static func instance() -> EditorScreen:
	var parent : GameScreen = GameScreen.instance()
	return null if parent == null else parent.editor_screen as EditorScreen

#######################################
### Private Variables
##############################

@export var _index_list : ItemList
@export var _contents   : Panel

var has_changes         : bool = false

func _ready() -> void:
	# only open to developers
	if not Session.is_access_at_least(Globals.Access.developer):
		_terminate()
		return
	
	# tell the server we entered editor
	Network.send_editor_opened()
	
	# setup the editor
	refresh_index_list()
	load_data_on_index(_contents.current_index)

func _terminate() -> void:
		self.visible = false
		self.queue_free()

#######################################
### Protected Accessors
##############################

func refresh_index_list() -> void:
	_index_list.clear()
	
	var db : Array = _contents.get_database()
	
	for i in db.size():
		var text : String = "%d: %s" % [i+1, db[i].name]
		_index_list.add_item(text)
	
	# target the current index selected
	_index_list.select(_contents.current_index)

func load_data_on_index(index: int) -> void:
	_contents.current_index = index
	_contents.load_database()
	has_changes = false

func try_session_escape(task: Callable) -> void:
	if not has_changes:
		if not task.is_null():
			task.call()
		return
	
	# warn of progress loss
	Overlay.message_box(
		"You have unsaved changes... continue?",
		"Unsaved changes warning",
		MessageBox.Buttons.yes_no,
		MessageBox.Icons.warning,
		func(r: MessageBox.Response):
			if r == MessageBox.Response.positive:
				if not task.is_null(): task.call()
			else: _index_list.select(_contents.current_index)
	)

#######################################
### Gui Functions
##############################

func _on_index_list_item_selected(index: int)  -> void: try_session_escape(func(): load_data_on_index(index))

func _on_refresh_pressed()                     -> void: refresh_index_list()
func _on_clear_changes_pressed()               -> void: load_data_on_index(_contents.current_index)
func _on_save_pressed()                        -> void: _contents.save_database(); has_changes = false
func _on_cancel_pressed()                      -> void: try_session_escape(func(): Network.send_editor_closed(); _terminate())

func _on_mouse_pressed() -> void: Audio.play_sound(Session.options.button_down)
func _on_mouse_entered() -> void: Audio.play_sound(Session.options.button_hover)
