class_name MenuScreen extends Control
static func instance() -> MenuScreen:
	for screen in Session.get_scene().get_children():
		if screen is MenuScreen: return screen
	return null

@export var _screen_title    : Label

@export var _panel_container : PanelContainer
@export var _credits         : CreditsPanel
@export var _register        : RegisterPanel
@export var _login           : LoginPanel
@export var _news            : NewsPanel
@export var _server_status   : Label

var _tmr_connection : float
var _is_connected   : bool

func _ready() -> void:
	# load up custom gui image
	($Background as TextureRect).texture = AssetLoader.load_gui("menu/Background")
	
	# start menu music
	Audio.play_music(Session.options.menu_music)
	
	# present the main panel
	set_visible_panel(_news)

func _physics_process(delta : float):
	# only perform if there is a need to reconnect
	if _is_connected and _is_connected == Network.socket_connected(): return
	
	# push timer
	_tmr_connection += delta
	
	# perform reconnect routine
	if _tmr_connection > 0.0:
		_tmr_connection = -5.0
		
		# kill status if we aren't connected or stable
		Overlay.clear_status(false)
		
		# attempt reconnect
		Network.connect_to_server()
	
	# resync status
	_is_connected = Network.socket_connected()
	
	# display status
	if _is_connected:
		_server_status.text = "MENU_STAT_ON"
		_server_status.self_modulate = Color.GREEN
	else:
		_server_status.text = "MENU_STAT_OFF"
		_server_status.self_modulate = Color.RED

#######################################
### Gui Functions
##############################

func _on_login_pressed()    -> void: set_visible_panel(_login)
func _on_register_pressed() -> void: set_visible_panel(_register)
func _on_credits_pressed()  -> void: set_visible_panel(_credits)
func _on_exit_pressed()     -> void: Session.terminate()

func set_visible_panel(target: Panel) -> void:
	if target.visible: target = _news
	
	for pnl in _panel_container.get_children():
		pnl.visible = false
	
	target.visible = true
	_screen_title.text = "MENU_" + target.name.to_upper()

func _on_mouse_pressed() -> void: Audio.play_sound(Session.options.button_down)
func _on_mouse_entered() -> void: Audio.play_sound(Session.options.button_hover)
