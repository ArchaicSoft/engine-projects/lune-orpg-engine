class_name GameScreen extends Control
static func instance() -> GameScreen:
	for screen in Session.get_scene().get_children():
		if screen is GameScreen: return screen
	return null

@export var game_view  : GameView
@export var hot_bar    : HotBar
@export var chat_box   : ChatBox
@export var hud        : HUD
@export var actions    : ActionContainer

@export var bank_window   : BankPanel

@export var editor_screen  : EditorScreen

func open_editor(editor_name: String) -> void:
	if editor_screen != null: return
	
	var path : String = "res://scenes/screens/editors/%sEditor.tscn" % editor_name
	if not FileAccess.file_exists(path): return
	
	editor_screen = load(path).instantiate()
	self.add_child(editor_screen)

func _ready() -> void:
	# load up custom gui image
	($Background as TextureRect).texture = AssetLoader.load_gui("game/Background")

# local variables
var requested_movement  : bool                  = false
var requested_move_dir  : Globals.Direction = Globals.Direction.down
var requested_move_type : Globals.Movement  = Globals.Movement.standing

func _input(_event: InputEvent) -> void:
	if chat_box.chat_input.has_focus(): return
	
	# check if need to focus chat box
	if Input.is_key_pressed(KEY_ENTER):
		chat_box.chat_input.grab_focus()
		return
	
	if Input.is_key_pressed(Globals.INPUT_OPEN_CONTROL_PANEL):
		actions.set_visible_panel(actions.control_panel)
	
	#######################
	# get keyboard input
	###################
	
	_check_movement() # process movement requests

func _check_movement() -> void:
	var dir_strength      : Vector2i = Vector2i.ZERO
	var request_movement  : bool = false
	var request_move_dir  : Globals.Direction = Globals.Direction.down
	var request_move_type : Globals.Movement  = Globals.Movement.standing
	
	# dominant up/down
	if Input.is_action_pressed("ui_left"):    dir_strength.x -= 1
	if Input.is_action_pressed("ui_right"):  dir_strength.x += 1
	if Input.is_action_pressed("ui_up"):  dir_strength.y -= 1
	if Input.is_action_pressed("ui_down"): dir_strength.y += 1
	
	# assign dir if they commited
	if dir_strength.y != 0:
		request_movement = true
		if dir_strength.y < 0: 
			request_move_dir   = Globals.Direction.up
		else: request_move_dir = Globals.Direction.down
	elif dir_strength.x != 0:
		request_movement = true
		if dir_strength.x < 0: 
			request_move_dir   = Globals.Direction.left
		else: request_move_dir = Globals.Direction.right
	
	# determine if we trying to move
	if not request_movement:
		# don't change last request
		request_move_dir  = requested_move_dir
		request_move_type = requested_move_type
	else:
		# determine speed
		if Input.is_action_pressed("run_button"):
			request_move_type   = Globals.Movement.running
		else: request_move_type = Globals.Movement.walking
	
	# check send move request
	if (
		request_movement != requested_movement or
		request_move_dir != requested_move_dir or
		request_move_type != requested_move_type
	):
		# update our last request info
		requested_movement  = request_movement
		requested_move_dir  = request_move_dir
		requested_move_type = request_move_type
		
		# send request
		Network.send_request_move(
			request_movement,
			request_move_dir,
			request_move_type
		)
