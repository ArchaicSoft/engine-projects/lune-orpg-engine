class_name ScoreLabel
extends ColorRect

@export var score_icon  : TextureRect
@export var score_name  : Label
@export var score_value : Label
