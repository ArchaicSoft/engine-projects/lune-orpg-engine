class_name IconBox
extends TextureRect

@onready var icon : TextureRect = $Icon

# what is it holding
var index : int

func set_icon(tex_image : Texture2D) -> void:
	icon.texture = tex_image
