class_name HotBar extends HBoxContainer
static func instance() -> HotBar:
	var parent : GameScreen = GameScreen.instance()
	return null if parent == null else parent.hot_bar as HotBar

var _slots : Array[TextureRect]

func _ready() -> void:
	for obj in self.get_children():
		if obj is TextureRect:
			_slots.append(obj)

func _get_slot(index: int) -> TextureRect:
	if index < 1 or index > _slots.size():
		return null
	
	return _slots[index-1]
