class_name HUD extends Panel
static func instance() -> HUD:
	var parent : GameScreen = GameScreen.instance()
	return null if parent == null else parent.hud as HUD

@export var _hp  : Control
@export var _mp  : Control
@export var _xp : Control

func get_hp() -> StatusBar: return _hp as StatusBar
func get_mp() -> StatusBar: return _mp as StatusBar
func get_xp() -> StatusBar: return _xp as StatusBar

func _physics_process(_delta: float) -> void:
	var vitals    : Array[int] = Session.my_player().character.vitals
	var cur_vital : int        = 0
	
	for vital_bar in [_hp, _mp, _xp]:
		var cur_val = vitals[cur_vital]
		var max_val = 100
		vital_bar.status_value.text = "%s/%s" % [cur_val, max_val]
		vital_bar.status_bar.value  = cur_val / max_val
