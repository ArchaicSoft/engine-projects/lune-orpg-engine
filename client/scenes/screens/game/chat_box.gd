class_name ChatBox extends Panel
static func instance() -> ChatBox:
	var parent : GameScreen = GameScreen.instance()
	return null if parent == null else parent.chat_box as ChatBox

@export var chat_text  : RichTextLabel
@export var chat_input : LineEdit

func _ready() -> void:
	chat_input.max_length = Globals.MAX_CHAT_INPUT

func _on_text_meta_clicked(meta: Variant) -> void:
	OS.shell_open(str(meta)) # open links in browser

func _on_input_text_submitted(new_text: String) -> void:
	var message : String = new_text.strip_escapes()
	
	# must have real text to send
	if message.length() < 1:
		_deactivate_input()
		return
	
	# check if its a command
	if message.begins_with("/"):
		Session.parse_command(message)
	
	# it is actual message content
	else:
		message = BB_Text.trim_spaces(message)
		
		# must have real text to send
		if not BB_Text.has_text_excluding_tags(message):
			return
		
		# clean up bb_code tags
		message = BB_Text.clean_tags(message)
		
		# send message and reset input box
		Network.send_chat(message)
	
	_deactivate_input()

func _deactivate_input() -> void:
	chat_input.clear()
	chat_input.release_focus()

