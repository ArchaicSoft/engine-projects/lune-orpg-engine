class_name OptionsPanel extends Panel
static func instance() -> OptionsPanel:
	var parent : ActionContainer = ActionContainer.instance()
	return null if parent == null else parent.options as OptionsPanel
