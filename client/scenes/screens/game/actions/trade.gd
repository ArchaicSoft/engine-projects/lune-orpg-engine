class_name TradePanel extends Panel
static func instance() -> TradePanel:
	var parent : ActionContainer = ActionContainer.instance()
	return null if parent == null else parent.trade as TradePanel
