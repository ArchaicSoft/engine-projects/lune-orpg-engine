class_name ActionContainer extends Panel
static func instance() -> ActionContainer:
	var parent : GameScreen = GameScreen.instance()
	return null if parent == null else parent.actions as ActionContainer

@export var action_bar       : ActionBar

@export var _panel_container : PanelContainer
@export var inventory        : InventoryPanel
@export var skills           : SkillsPanel
@export var character        : CharacterPanel
@export var options          : OptionsPanel
@export var trade            : TradePanel
@export var party            : PartyPanel

@export var control_panel    : ControlPanel

func _ready() -> void:
	set_visible_panel(inventory)

func set_visible_panel(target: Panel) -> void:
	for pnl in _panel_container.get_children():
		pnl.visible = false
	
	target.visible = true
