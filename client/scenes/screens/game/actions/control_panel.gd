class_name ControlPanel extends Panel

func _ready():
	visible = false

func _on_visibility_changed():
	if not visible: return
	
	# only open to staff
	if not Session.is_access_at_least(Globals.Access.moderator):
		var actions : ActionContainer = ActionContainer.instance()
		actions.set_visible_panel(actions.options)
		return

func _on_editors_animation_pressed() -> void:
	GameScreen.instance().open_editor("Animation")

func _on_editors_class_pressed() -> void:
	GameScreen.instance().open_editor("Class")

func _on_editors_item_pressed() -> void:
	GameScreen.instance().open_editor("Item")

func _on_editors_map_pressed() -> void:
	GameScreen.instance().open_editor("Map")

func _on_editors_npc_pressed() -> void:
	GameScreen.instance().open_editor("Npc")

func _on_editors_shop_pressed() -> void:
	GameScreen.instance().open_editor("Shop")

func _on_editors_spell_pressed() -> void:
	GameScreen.instance().open_editor("Spell")

func _on_editors_supply_pressed() -> void:
	GameScreen.instance().open_editor("Supply")

func _on_mouse_pressed() -> void: Audio.play_sound(Session.options.button_down)
func _on_mouse_entered() -> void: Audio.play_sound(Session.options.button_hover)
