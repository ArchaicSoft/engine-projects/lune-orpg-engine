class_name ActionBar extends Panel
static func instance() -> ActionBar:
	var parent : ActionContainer = ActionContainer.instance()
	return null if parent == null else parent.action_bar as ActionBar

@onready var _parent := ActionContainer.instance()

func _on_inventory_pressed():
	_parent.set_visible_panel(_parent.inventory)

func _on_skills_pressed():
	_parent.set_visible_panel(_parent.skills)

func _on_status_pressed():
	_parent.set_visible_panel(_parent.character)

func _on_options_pressed():
	_parent.set_visible_panel(_parent.options)

func _on_trade_pressed():
	_parent.set_visible_panel(_parent.trade)

func _on_party_pressed():
	_parent.set_visible_panel(_parent.party)

func _on_mouse_pressed() -> void: Audio.play_sound(Session.options.button_down)
func _on_mouse_entered() -> void: Audio.play_sound(Session.options.button_hover)
