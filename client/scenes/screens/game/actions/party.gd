class_name PartyPanel extends Panel
static func instance() -> PartyPanel:
	var parent : ActionContainer = ActionContainer.instance()
	return null if parent == null else parent.party as PartyPanel
