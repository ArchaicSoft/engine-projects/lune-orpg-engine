class_name SkillsPanel extends Panel
static func instance() -> SkillsPanel:
	var parent : ActionContainer = ActionContainer.instance()
	return null if parent == null else parent.skills as SkillsPanel
