class_name CharacterPanel extends Panel
static func instance() -> CharacterPanel:
	var parent : ActionContainer = ActionContainer.instance()
	return null if parent == null else parent.character as CharacterPanel
