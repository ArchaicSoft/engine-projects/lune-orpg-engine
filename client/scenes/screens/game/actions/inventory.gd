class_name InventoryPanel extends Panel
static func instance() -> InventoryPanel:
	var parent : ActionContainer = ActionContainer.instance()
	return null if parent == null else parent.inventory as InventoryPanel

var slot_scene : PackedScene = load("res://scenes/screens/game/components/IconBox.tscn")

var slots : Array[IconBox]

func _ready() -> void:
	var grid     : GridContainer = $Scrollable/Grid
	var inv_size : int           = Globals.Max.inventory
	slots.resize(inv_size)
	
	for i in inv_size:
		var slot : IconBox = slot_scene.instantiate()
		slot.name = str(i)
		slot.index = -1
		slots[i] = slot
		grid.add_child(slot)

# todo : Add a on_resize event and adjust columns by it
