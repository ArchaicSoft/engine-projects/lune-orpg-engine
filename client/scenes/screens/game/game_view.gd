class_name GameView extends SubViewportContainer
static func instance() -> GameView:
	var parent : GameScreen = GameScreen.instance()
	return null if parent == null else parent.game_view as GameView

@export var viewport : SubViewport
@export var world    : Node2D
