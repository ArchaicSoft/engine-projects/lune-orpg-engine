class_name BankPanel extends Panel
static func instance() -> BankPanel:
	var parent : GameScreen = GameScreen.instance()
	return null if parent == null else parent.bank as BankPanel
