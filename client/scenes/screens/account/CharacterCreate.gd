class_name CharacterCreatePanel extends Panel
static func instance() -> CharacterCreatePanel:
	var parent : AccountScreen = AccountScreen.instance()
	return null if parent == null else parent.character_create as CharacterCreatePanel

@export var _name_box     : LineEdit
@export var _classes_list : OptionButton
@export var _sex_button   : Button
@export var _skin_label   : Label
@export var _sprite       : Sprite2D

var _sex_value            : Globals.Sex = Globals.Sex.male
var _skin_value           : int         = 0

func _refresh_classes() -> void:
	_classes_list.clear()
	
	var classes : Array[Class] = Database.classes
	for i in classes.size():
		_classes_list.add_item(classes[i].name)

func _set_sex(sex: Globals.Sex) -> void:
	# modify sex button and value
	_sex_value       = sex
	_sex_button.text = Globals.Sex.keys()[sex].capitalize()
	
	# reset sprite to sex's first sprite
	var classes  :     = Database.classes
	var class_id : int = _classes_list.get_selected_id()
	
	# ensure there's a sprite to set
	if classes.size() < 1 or class_id < 0: return
	
	# assign sprite
	match sex:
		Globals.Sex.male:   _set_skin(classes[class_id].sprite_male[0])
		Globals.Sex.female: _set_skin(classes[class_id].sprite_female[0])

func _set_skin(index: int) -> void:
	# get class info
	var classes      :            = Database.classes
	var class_id     : int        = _classes_list.get_selected_id()
	var sprite_array : Array[int]
	
	match _sex_value:
		Globals.Sex.male:   sprite_array = classes[class_id].sprite_male
		Globals.Sex.female: sprite_array = classes[class_id].sprite_female
	
	# find the correct in-bound index
	if   index < 0: index = sprite_array.size() - 1
	elif index >= sprite_array.size(): index = 0
	
	# modify skin label and value
	_skin_value       = index
	_skin_label.text  = str(index)
	
	# display sprite
	_sprite.texture = AssetLoader.load_player(sprite_array[index])

#######################################
### Gui Functions
##############################

func _on_character_creator_visibility_changed() -> void:
	if not self.visible: return
	
	# default some values
	_name_box.text = ""
	_refresh_classes()
	_set_sex(Globals.Sex.male)

func _on_sex_button_pressed() -> void:
	match _sex_value:
		Globals.Sex.male:   _set_sex(Globals.Sex.female)
		Globals.Sex.female: _set_sex(Globals.Sex.male)

func _on_skin_previous_pressed() -> void:
	_set_skin(_skin_value - 1)

func _on_skin_next_pressed() -> void:
	_set_skin(_skin_value + 1)

func _on_accept_pressed() -> void:
	if not Utility.is_alpha_numeric(_name_box.text):
		Overlay.message_box( 
			"Character name must contain only letters and numbers.",
			"Invalid character name"
		); return
	
	# ensure valid before classes are created.
	var class_id : int = _classes_list.get_selected_id()
	if class_id < 0: class_id = 0
	
	Network.send_character_create(
		_name_box.text,
		_sex_value,
		class_id,
		_skin_value
	)

func _on_cancel_pressed() -> void:
	var account_screen : AccountScreen = Session.get_screen() as AccountScreen
	account_screen.set_visible_panel(account_screen.character_select)

func _on_mouse_pressed() -> void: Audio.play_sound(Session.options.button_down)
func _on_mouse_entered() -> void: Audio.play_sound(Session.options.button_hover)
