class_name CharacterSelectPanel extends Panel
static func instance() -> CharacterSelectPanel:
	var parent : AccountScreen = AccountScreen.instance()
	return null if parent == null else parent.character_select as CharacterSelectPanel

@export var _name  : Label
@export var _class : Label
@export var _level : Label
@export var _map   : Label
@export var _accept : Button
@export var _delete : Button

func _on_character_selection_visibility_changed() -> void:
	if not self.visible: return
	
	var player    : Player    = Session.my_player()
	var character : Character = player.character
	
	# display character info
	if character == null:
		_name.text   = "Not Available"
		_class.text  = "Not Available"
		_level.text  = "Not Available"
		_map.text    = "Not Available"
		_accept.text = "Create"
		_delete.disabled = true
	else: # has character data
		_name.text   = character.name
		_class.text  = Database.classes[character.class_type].name
		_level.text  = str(character.level)
		_map.text    = Database.maps[character.map].name
		_accept.text = "Play"
		_delete.disabled = false

func _on_accept_pressed() -> void:
	if Session.my_player().character == null:
		var account_screen : AccountScreen = AccountScreen.instance()
		account_screen.set_visible_panel(account_screen.character_create)
	else:
		Network.send_character_select()

func _on_delete_pressed() -> void:
	Network.send_character_delete()

func _on_mouse_pressed() -> void: Audio.play_sound(Session.options.button_down)
func _on_mouse_entered() -> void: Audio.play_sound(Session.options.button_hover)
