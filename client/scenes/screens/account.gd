class_name AccountScreen extends Control
static func instance() -> AccountScreen:
	for screen in Session.get_scene().get_children():
		if screen is AccountScreen: return screen
	return null

@export var _screen_title    : Label

@export var _panel_container  : PanelContainer
@export var character_create : CharacterCreatePanel
@export var character_select : CharacterSelectPanel

func _ready() -> void:
	# load up custom gui image
	($Background as TextureRect).texture = AssetLoader.load_gui("account/Background")
	
	# present the main panel
	set_visible_panel(character_select)

func _physics_process(_delta : float):
	# send us back to main menu and ensure connection is closed
	if not Network.socket_connected():
		Network.socket_disconnect()

#######################################
### Gui Functions
##############################

func _on_logout_pressed() -> void: Network.socket_disconnect()
func _on_quit_pressed()   -> void: Session.terminate()

func set_visible_panel(target: Panel) -> void:
	if target.visible: target = character_select
	
	for pnl in _panel_container.get_children():
		pnl.visible = false
	
	target.visible = true
	_screen_title.text = target.name

func _on_mouse_pressed() -> void: Audio.play_sound(Session.options.button_down)
func _on_mouse_entered() -> void: Audio.play_sound(Session.options.button_hover)
