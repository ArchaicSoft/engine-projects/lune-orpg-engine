class_name LoginPanel
extends Panel

@export var _username      : LineEdit
@export var _password      : LineEdit
@export var _save_username : CheckBox
@export var _save_password : CheckBox

func _ready() -> void:
	_username.max_length = Globals.MAX_USERNAME
	_password.max_length = Globals.MAX_PASSWORD

#######################################
### Gui Functions
##############################

func _on_visibility_changed() -> void:
	if not self.visible: return
	
	_username.grab_focus()
	if Session.options.save_user:
		_save_username.button_pressed = true
		_username.text = Session.options.username
	
	if Session.options.save_pass:
		_save_password.button_pressed = true
		_password.text = Session.options.password

func _on_username_text_text_submitted(_new_text: String) -> void:
	_on_submit_pressed()

func _on_password_text_text_submitted(_new_text: String) -> void:
	_on_submit_pressed()

func _on_save_username_toggled(button_pressed: bool) -> void:
	_on_mouse_pressed()
	Session.options.save_user = button_pressed

func _on_save_password_toggled(button_pressed: bool) -> void:
	_on_mouse_pressed()
	Session.options.save_pass = button_pressed

func _on_submit_pressed() -> void:
	var username : String = _username.text.strip_escapes()
	var password : String = _password.text.strip_escapes()
	
	if not (
		Globals.MIN_USERNAME <= username.length() and username.length() <= Globals.MAX_USERNAME and 
		Globals.MIN_PASSWORD <= password.length() and password.length() <= Globals.MAX_PASSWORD
	):
		Overlay.message_box( 
			("Username or Password does not meet size requirements! " +
			"Username must be between(%d, %d) and " +
			"Password must be between(%d, %d).") % 
			[Globals.MIN_USERNAME, Globals.MAX_USERNAME,
			Globals.MIN_PASSWORD, Globals.MAX_PASSWORD],
			"Invalid username or password"
		); return
	
	if not Utility.is_alpha_numeric(username):
		Overlay.message_box( 
			"Username must contain only letters and numbers.",
			"Invalid username"
		); return
	
	Network.send_login(username, password)
	
	# save our username and password if applicable
	
	if Session.options.save_user:
		Session.options.username = username
	
	if Session.options.save_pass:
		Session.options.password = password
	
	Session.options.save_self()

func _on_cancel_pressed() -> void:
	MenuScreen.instance().set_visible_panel(self)

func _on_mouse_pressed() -> void: Audio.play_sound(Session.options.button_down)
func _on_mouse_entered() -> void: Audio.play_sound(Session.options.button_hover)
