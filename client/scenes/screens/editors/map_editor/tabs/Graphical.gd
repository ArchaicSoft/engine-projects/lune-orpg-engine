class_name MapEditor_GraphicalTab extends VBoxContainer

#######################################
### Calldowns
##############################

func load_database(editor: EditorContents, map: Map) -> void:
	_editor = editor; _cur_map = map

#######################################
### Local Handlers
##############################

# controls
@export var _tileset_render : TextureRect

@export var _tileset_label  : Label
@export var _tileset_scroll : HScrollBar
@export var _layer_label    : Label
@export var _layer_scroll   : HScrollBar

# assets
var _tilesets               : Array[Texture2D]

# editor state values
var _editor                 : EditorContents
var _cur_map                : Map

var cur_gfx_rect            :     = Rect2i(0, 0, Globals.TILE_SIZE, Globals.TILE_SIZE)
var cur_tileset             : int = 0
var cur_layer               : int = 0

func _ready() -> void:
	var cur_texture : int = 0
	
	# loop-load textures until no more are found
	while true:
	# try to load texture
		var texture : Texture2D = AssetLoader.load_tileset(cur_texture, false)
		if texture == null: break
		
		# add the texture to array
		_tilesets.append(texture)
		cur_texture += 1
	
	# update gui
	_tileset_scroll.max_value = cur_texture - 1
	_on_tileset_index_scroll_value_changed(0)
	
	_layer_scroll.max_value = Globals.MapLayer.keys().size() - 1
	_on_layer_index_scroll_value_changed(0)

#######################################
### Gui Functions
##############################

func _on_tileset_index_scroll_value_changed(value: float) -> void:
	cur_tileset = int(value)
	_tileset_label.text = "Tileset: [%d]" % cur_tileset
	_tileset_render.texture = _tilesets[cur_tileset]


func _on_layer_index_scroll_value_changed(value: float) -> void:
	cur_layer = int(value)
	_layer_label.text = "Layer: [%s]" % Globals.MapLayer.keys()[cur_layer]
