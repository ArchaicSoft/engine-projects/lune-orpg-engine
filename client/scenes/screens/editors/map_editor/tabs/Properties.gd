class_name MapEditor_PropertiesTab extends ScrollContainer

#######################################
### Calldowns
##############################

func load_database(editor: EditorContents, map: Map) -> void:
	_editor = editor; _cur_map = map
	
	_reload_list_arrays()
	
	_name_input.text = map.name
	_type_value.select(map.type)
	
	_can_resize = false
	_size_x_value.value = map.size.x
	_size_y_value.value = map.size.y
	_can_resize = true
	
	_death_map.select(map.death_map + 1)
	_death_x_value.value = map.death_pos.x
	_death_y_value.value = map.death_pos.y
	
#	_connected_n_w.select(map.connected_map[Globals.Direction.up_left]    + 1)
	_connected_n.select(  map.connected_map[Globals.Direction.up]         + 1)
#	_connected_n_e.select(map.connected_map[Globals.Direction.up_right]   + 1)
	_connected_w.select(  map.connected_map[Globals.Direction.left]       + 1)
	_connected_current.text = str(_editor.current_index                       + 1)
	_connected_e.select(  map.connected_map[Globals.Direction.right]      + 1)
#	_connected_s_w.select(map.connected_map[Globals.Direction.down_left]  + 1)
	_connected_s.select(  map.connected_map[Globals.Direction.down]       + 1)
#	_connected_s_e.select(map.connected_map[Globals.Direction.down_right] + 1)

#######################################
### Local Handlers
##############################

# controls
@export var _name_input        : LineEdit
@export var _type_value        : OptionButton

@export var _size_x_value      : SpinBox
@export var _size_y_value      : SpinBox

@export var _death_map         : OptionButton
@export var _death_x_value     : SpinBox
@export var _death_y_value     : SpinBox

@export var _connected_n_w     : OptionButton
@export var _connected_n       : OptionButton
@export var _connected_n_e     : OptionButton
@export var _connected_w       : OptionButton
@export var _connected_current : Label
@export var _connected_e       : OptionButton
@export var _connected_s_w     : OptionButton
@export var _connected_s       : OptionButton
@export var _connected_s_e     : OptionButton

@export var _music_list        : ItemList

# editor state values
var _editor                    : EditorContents
var _cur_map                   : Map
var _can_resize                : bool

func _ready() -> void:
	for key in Globals.Map.keys():
		_type_value.add_item(key.capitalize())
	
	# todo: check external directory and sub directories, also
	#       make it only provide each listing once if duplicates
	scan_music_files(Paths.audio_music(false))

func _reload_list_arrays() -> void:
	var map_array :     = Database.maps
	var max_maps  : int = Globals.Max.maps
	
	_editor.database_to_list(_death_map,     map_array, max_maps, true)
	
	_editor.database_to_list(_connected_n_w, map_array, max_maps, true)
	_editor.database_to_list(_connected_n,   map_array, max_maps, true)
	_editor.database_to_list(_connected_n_e, map_array, max_maps, true)
	_editor.database_to_list(_connected_w,   map_array, max_maps, true)
	_editor.database_to_list(_connected_e,   map_array, max_maps, true)
	_editor.database_to_list(_connected_s_w, map_array, max_maps, true)
	_editor.database_to_list(_connected_s,   map_array, max_maps, true)
	_editor.database_to_list(_connected_s_e, map_array, max_maps, true)

func scan_music_files(path: String) -> void:
	var dir := DirAccess.open(path)
	if dir == null: return
	
	var file_list := dir.get_files()
	for file in file_list:
		if (
			file.get_extension() == "mp3" or
			file.get_extension() == "ogg" or
			file.get_extension() == "wav"
		): _music_list.add_item(file)

func resize_map() -> void:
	if not _can_resize: return
	
	var tile_len  : int = _cur_map.tiles.size()
	var true_size : int = _cur_map.size.x * _cur_map.size.y
	
	# make sure there is work to be done
	if tile_len == true_size: return
	_cur_map.tiles.resize(true_size)
	
	# only continue if there is new tiles to init
	if tile_len > true_size: return
	
	# init tiles
	for i in range(tile_len, true_size):
		_cur_map.tiles[i] = Tile.new()

#######################################
### Gui Functions
##############################

# general

func _on_name_input_text_changed(new_text: String) -> void:
	_cur_map.name = new_text
	
	_editor.notify_changes()

func _on_type_value_item_selected(index: int) -> void:
	_cur_map.type = (index as Globals.Map)
	
	_editor.notify_changes()

# size

func _on_size_x_value_value_changed(value: float) -> void:
	_cur_map.size.x = int(value); resize_map()
	
	_editor.resize_map_view()
	_editor.notify_changes()

func _on_size_y_value_value_changed(value: float) -> void:
	_cur_map.size.y = int(value); resize_map()
	
	_editor.resize_map_view()
	_editor.notify_changes()

# death map

func _on_death_map_item_selected(index: int) -> void:
	_cur_map.death_map = index - 1
	
	_editor.notify_changes()

func _on_death_x_value_changed(value: float) -> void:
	_cur_map.death_pos.x = int(value)
	
	_editor.notify_changes()

func _on_death_y_value_changed(value: float) -> void:
	_cur_map.death_pos.y = int(value)
	
	_editor.notify_changes()

# connected maps

func _on_north_west_item_selected(index: int) -> void:
#	_cur_map.connected_map[Globals.Direction.up_left] = index - 1
	
	_editor.notify_changes()

func _on_north_item_selected(index: int) -> void:
	_cur_map.connected_map[Globals.Direction.up] = index - 1
	
	_editor.notify_changes()

func _on_north_east_item_selected(index: int) -> void:
#	_cur_map.connected_map[Globals.Direction.up_right] = index - 1
	
	_editor.notify_changes()

func _on_west_item_selected(index: int) -> void:
	_cur_map.connected_map[Globals.Direction.left] = index - 1
	
	_editor.notify_changes()

func _on_east_item_selected(index: int) -> void:
	_cur_map.connected_map[Globals.Direction.right] = index - 1
	
	_editor.notify_changes()

func _on_south_west_item_selected(index: int) -> void:
#	_cur_map.connected_map[Globals.Direction.down_left] = index - 1
	
	_editor.notify_changes()

func _on_south_item_selected(index: int) -> void:
	_cur_map.connected_map[Globals.Direction.down] = index - 1
	
	_editor.notify_changes()

func _on_south_east_item_selected(index: int) -> void:
#	_cur_map.connected_map[Globals.Direction.down_right] = index - 1
	
	_editor.notify_changes()

func _on_mouse_pressed() -> void: Audio.play_sound(Session.options.button_down)
func _on_mouse_entered() -> void: Audio.play_sound(Session.options.button_hover)
