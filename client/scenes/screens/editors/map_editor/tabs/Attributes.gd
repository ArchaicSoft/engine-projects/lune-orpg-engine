class_name MapEditor_AttributesTab extends VBoxContainer

#######################################
### Calldowns
##############################

func load_database(editor: EditorContents, map: Map) -> void:
	_editor = editor; _cur_map = map
	
	_reload_list_arrays()
	

#######################################
### Local Handlers
##############################

# controls
@export var _current_attribute   : Label

#@export block data

@export var _item_image          : TextureRect
@export var _item_info_label     : Label
@export var _item_index          : OptionButton
@export var _item_amount_scroll  : HScrollBar
@export var _item_respawn_scroll : HScrollBar

#@export bank data

@export var _shop_index          : OptionButton

@export var _slide_direction     : OptionButton

@export var _warp_map            : OptionButton
@export var _warp_x_value        : SpinBox
@export var _warp_y_value        : SpinBox
@export var _warp_direction      : OptionButton

@export var _vital_mod_hp_value  : SpinBox
@export var _vital_mod_mp_value  : SpinBox

@export var _lock_image          : TextureRect
@export var _lock_info_label     : Label
@export var _lock_index          : OptionButton
@export var _lock_amount_scroll  : HScrollBar
@export var _lock_destroy_value  : CheckBox

@export var _unlock_map          : OptionButton
@export var _unlock_x_value      : SpinBox
@export var _unlock_y_value      : SpinBox

#@export door (NOT IMPLEMENTED)

@export var _supply_index        : OptionButton

#@export npc avoid

@export var _npc_spawn_index     : OptionButton
@export var _npc_spawn_direction : OptionButton

# editor state values
var _editor                      : EditorContents
var _cur_map                     : Map
var _attribute_data              : Tile

func _ready() -> void:
	for key in Globals.Direction.keys():
		var dir_str : String = key.capitalize()
		
		_slide_direction.add_item(dir_str)
		_warp_direction.add_item(dir_str)
		_npc_spawn_direction.add_item(dir_str)
	
	_on_block_tile_select_pressed()

func _reload_list_arrays() -> void:
	_editor.database_to_list(_item_index,      Database.items,     Globals.Max.items)
	_editor.database_to_list(_shop_index,      Database.shops,     Globals.Max.shops)
	_editor.database_to_list(_warp_map,        Database.maps,      Globals.Max.maps)
	_editor.database_to_list(_lock_index,      Database.items,     Globals.Max.items)
	_editor.database_to_list(_unlock_map,      Database.maps,      Globals.Max.maps)
	# [door list array here]
	_editor.database_to_list(_supply_index,    Database.supplies,  Globals.Max.supplies)
	_editor.database_to_list(_npc_spawn_index, Database.npcs,      Globals.Max.npcs)

func _describe_attribute(info: String) -> void:
	var tile_type : String = Globals.Tile.keys()[_attribute_data.type].capitalize()
	_current_attribute.text = "Current: [%s | %s]" % [tile_type, info]

func _set_attribute(
	type   : Globals.Tile,
	data_1 : int = 0,
	data_2 : int = 0,
	data_3 : int = 0
) -> void:
	var tile_data := Tile.new()
	tile_data.type = type
	tile_data.data_1 = data_1
	tile_data.data_2 = data_2
	tile_data.data_3 = data_3
	_attribute_data = tile_data

#######################################
### Gui Functions
##############################

# block

func _on_block_directional_select_pressed() -> void:
	_set_attribute(Globals.Tile.block)
	_describe_attribute("Directional")

func _on_block_tile_select_pressed() -> void:
	_set_attribute(Globals.Tile.block)
	_describe_attribute("Tile")

# item

func _on_item_select_pressed() -> void:
	var item_index  : int    = _item_index.selected
	var item_amount : int    = int(_item_amount_scroll.value)
	var item_timer  : int    = int(_item_respawn_scroll.value)
	var item_name   : String = Database.items[item_index].name
	
	_set_attribute(Globals.Tile.item, item_index, item_amount, item_timer)
	_describe_attribute("%dx %s over %d secs" % [item_amount, item_name, item_timer])

func _display_item_info() -> void:
	var item_index  : int    = _item_index.selected
	var item_amount : int    = int(_item_amount_scroll.value)
	var item_timer  : int    = int(_item_respawn_scroll.value)
	var item_name   : String = Database.items[item_index].name
	
	_item_info_label.text = "%dx %s over %d secs" % [item_amount, item_name, item_timer]
	(_item_image as IconBox).set_icon(AssetLoader.load_item(item_index))

func _on_item_index_item_selected(index: int) -> void:
	_display_item_info()

func _on_item_amount_scroll_value_changed(value: float) -> void:
	_display_item_info()

func _on_item_respawn_timer_scroll_value_changed(value: float) -> void:
	_display_item_info()

# bank

func _on_bank_select_pressed() -> void:
	_set_attribute(Globals.Tile.bank)
	_describe_attribute("Storage")

# shop

func _on_shop_select_pressed() -> void:
	var shop_value : int    = _shop_index.selected
	var shop_name  : String = Database.shops[shop_value].name
	_set_attribute(Globals.Tile.shop, shop_value)
	_describe_attribute(shop_name)

func _on_shop_index_value_item_selected(index: int) -> void: pass

# slide

func _on_slide_select_pressed() -> void:
	var dir_value : int    = _slide_direction.selected
	var dir_name  : String = Globals.Direction.keys()[dir_value].capitalize()
	_set_attribute(Globals.Tile.slide, dir_value)
	_describe_attribute(dir_name)

func _on_slide_direction_value_item_selected(index: int) -> void: pass

# warp

func _on_warp_select_pressed() -> void:
	var warp_map      : int    = _warp_map.selected
	var warp_x        : int    = int(_warp_x_value.value)
	var warp_y        : int    = int(_warp_y_value.value)
	var warp_coords   : int    = (
									(warp_map & 0xFFFF) |
									((warp_x & 0xFFFF) << 0x10) |
									((warp_y & 0xFFFF) << 0x20)
								)
	var warp_dir      : int    = _warp_direction.selected
	var warp_dir_name : String = Globals.Direction.keys()[warp_dir]
	
	_set_attribute(Globals.Tile.warp, warp_coords, warp_dir)
	_describe_attribute("%s(%d, %d) facing %s" % [
													Database.maps[warp_map].name,
													warp_x,
													warp_y,
													warp_dir_name
												])

func _on_warp_map_item_selected(index: int) -> void: pass

func _on_warp_x_value_changed(value: float) -> void: pass

func _on_warp_y_value_changed(value: float) -> void: pass

func _on_warp_direction_value_item_selected(index: int) -> void: pass

# vital mod

func _on_vital_mod_select_pressed() -> void:
	pass # Replace with function body.

func _on_vital_mod_hp_value_changed(value: float) -> void:
	pass # Replace with function body.

func _on_vital_mod_mp_value_changed(value: float) -> void:
	pass # Replace with function body.

# lock

func _on_lock_select_pressed() -> void:
	pass # Replace with function body.

func _on_lock_index_item_selected(index: int) -> void:
	pass # Replace with function body.

func _on_lock_amount_scroll_value_changed(value: float) -> void:
	pass # Replace with function body.

func _on_lock_key_destroy_pressed() -> void:
	pass # Replace with function body.

# unlock

func _on_unlock_select_pressed() -> void:
	pass # Replace with function body.

func _on_unlock_map_item_selected(index: int) -> void:
	pass # Replace with function body.

func _on_unlock_x_value_changed(value: float) -> void:
	pass # Replace with function body.

func _on_unlock_y_value_changed(value: float) -> void:
	pass # Replace with function body.

# door

func _on_door_select_pressed() -> void:
	pass # Replace with function body.

func _on_door_index_value_item_selected(index: int) -> void:
	pass # Replace with function body.

# supply

func _on_supply_select_pressed() -> void:
	pass # Replace with function body.

func _on_supply_index_value_item_selected(index: int) -> void:
	pass # Replace with function body.

# npc avoid

func _on_npc_avoid_select_pressed() -> void:
	pass # Replace with function body.

# npc spawn

func _on_npc_spawn_select_pressed() -> void:
	pass # Replace with function body.

func _on_npc_spawn_index_value_item_selected(index: int) -> void:
	pass # Replace with function body.

func _on_npc_spawn_direction_value_item_selected(index: int) -> void:
	pass # Replace with function body.

func _on_mouse_pressed() -> void: Audio.play_sound(Session.options.button_down)
func _on_mouse_entered() -> void: Audio.play_sound(Session.options.button_hover)
