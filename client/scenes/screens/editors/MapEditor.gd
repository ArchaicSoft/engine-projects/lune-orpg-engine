extends EditorContents

#######################################
### Overrides
##############################

func get_database()  -> Array: return Database.maps

func load_database() -> void:
	# complicated, but ensures we modify a copy, not the original!
	_cur_map = str_to_var(var_to_str(Database.maps[current_index]))
	
	# pass loadout to tabs
	_graphical_tab.load_database( self, _cur_map)
	_attributes_tab.load_database(self, _cur_map)
	_properties_tab.load_database(self, _cur_map)
	
	# refresh map screen
	resize_map_view()

func save_database() -> void:
	Network.send_editor_saved(Globals.Editor.map, current_index, var_to_str(_cur_map))

#######################################
### Local Handlers
##############################

@export var _graphical_tab     : VBoxContainer
@export var _attributes_tab    : VBoxContainer
@export var _properties_tab    : ScrollContainer

@export var _map_container     : Panel
@export var _render_map        : TileMap
@export var _render_attributes : TileMap
@export var _render_direction  : TileMap

# editor state values
var _cur_map                   : Map

func _ready() -> void:
	# make map open to our current map first
	current_index = Session.my_player().character.map

func resize_map_view() -> void:
	_map_container.custom_minimum_size = _cur_map.size * Globals.TILE_SIZE

#######################################
### Gui Functions
##############################
