class_name EditorContents extends Panel

#######################################
### Must Overrides
##############################

func get_database()  -> Array: return Array()
func load_database() -> void:  pass
func save_database() -> void:  pass

#######################################
### Protected Accessors
##############################

var current_index       : int       = 0

func notify_changes() -> void: EditorScreen.instance().has_changes = true

func database_to_list(
	list      : OptionButton,
	db_array  : Array,
	db_size   : int,
	has_empty : bool = false
) -> void:
	var sel_index : int = list.selected
	
	list.clear();
	if has_empty:     list.add_item("0: N/A - Unselected")
	for i in db_size: list.add_item(str(i+1) + ": " + db_array[i].name)
	list.select(0 if sel_index < 0 else sel_index)
