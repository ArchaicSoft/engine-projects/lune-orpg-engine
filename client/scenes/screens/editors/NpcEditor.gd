extends EditorContents

#######################################
### Overrides
##############################

func get_database()  -> Array: return Database.npcs

func load_database() -> void:
	# complicated, but ensures we modify a copy, not the original!
	_cur_npc = str_to_var(var_to_str(Database.npcs[current_index]))

func save_database() -> void:
	Network.send_editor_saved(Globals.Editor.npc, current_index, var_to_str(_cur_npc))

#######################################
### Local Handlers
##############################

# assets
var _cur_npc : Npc

# editor state values
#

func _ready() -> void:
	pass

#######################################
### Gui Functions
##############################
