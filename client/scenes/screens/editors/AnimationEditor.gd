extends EditorContents

#######################################
### Overrides
##############################

func get_database()  -> Array: return Database.animations

func load_database() -> void:
	# complicated, but ensures we modify a copy, not the original!
	_cur_anim = str_to_var(var_to_str(Database.animations[current_index]))

func save_database() -> void:
	Network.send_editor_saved(Globals.Editor.animation, current_index, var_to_str(_cur_anim))

#######################################
### Local Handlers
##############################

# assets
var _cur_anim : Animation2D

# editor state values
#

func _ready() -> void:
	pass

#######################################
### Gui Functions
##############################
