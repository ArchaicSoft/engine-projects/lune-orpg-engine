extends EditorContents

#######################################
### Overrides
##############################

func get_database()  -> Array: return Database.items

func load_database() -> void:
	# complicated, but ensures we modify a copy, not the original!
	_cur_item = str_to_var(var_to_str(Database.items[current_index]))

func save_database() -> void:
	Network.send_editor_saved(Globals.Editor.item, current_index, var_to_str(_cur_item))

#######################################
### Local Handlers
##############################

# assets
var _cur_item : Item

# editor state values
#

func _ready() -> void:
	pass

#######################################
### Gui Functions
##############################
