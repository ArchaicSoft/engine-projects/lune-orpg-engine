extends EditorContents

#######################################
### Overrides
##############################

func get_database()  -> Array: return Database.supplies

func load_database() -> void:
	# complicated, but ensures we modify a copy, not the original!
	_cur_supply = str_to_var(var_to_str(Database.supplies[current_index]))

func save_database() -> void:
	Network.send_editor_saved(Globals.Editor.supply, current_index, var_to_str(_cur_supply))

#######################################
### Local Handlers
##############################

# assets
var _cur_supply : Supply

# editor state values
#

func _ready() -> void:
	pass

#######################################
### Gui Functions
##############################
