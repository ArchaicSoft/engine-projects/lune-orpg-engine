extends EditorContents

#######################################
### Overrides
##############################

func get_database()  -> Array: return Database.spells

func load_database() -> void:
	# complicated, but ensures we modify a copy, not the original!
	_cur_spell = str_to_var(var_to_str(Database.spells[current_index]))

func save_database() -> void:
	Network.send_editor_saved(Globals.Editor.spell, current_index, var_to_str(_cur_spell))

#######################################
### Local Handlers
##############################

# assets
var _cur_spell : Spell

# editor state values
#

func _ready() -> void:
	pass

#######################################
### Gui Functions
##############################
