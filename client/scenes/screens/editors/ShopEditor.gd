extends EditorContents

#######################################
### Overrides
##############################

func get_database()  -> Array: return Database.shops

func load_database() -> void:
	# complicated, but ensures we modify a copy, not the original!
	_cur_shop = str_to_var(var_to_str(Database.shops[current_index]))
	
	_name_input.text       = _cur_shop.name
	_buy_rate_scroll.value = _cur_shop.buy_rate
	
	_reload_list_arrays()
	
	_reload_item_list()
	_item_list.deselect_all()
	_on_item_list_item_selected(-1)

func save_database() -> void:
	Network.send_editor_saved(Globals.Editor.shop, current_index, var_to_str(_cur_shop))

#######################################
### Local Handlers
##############################

# controls
@export var _name_input          : LineEdit

@export var _buy_rate_label      : Label
@export var _buy_rate_scroll     : ScrollBar

@export var _item_list           : ItemList
@export var _item_add_button     : Button
@export var _item_del_button     : Button

@export var _item_receive_index  : OptionButton
@export var _item_receive_amount : SpinBox

@export var _item_require_index  : OptionButton
@export var _item_require_amount : SpinBox

@export var _item_update_button  : Button

# editor state values
var _cur_shop : Shop

func _ready() -> void:
	pass

func _reload_list_arrays() -> void:
	var item_array :     = Database.items
	var max_items  : int = Globals.Max.items
	
	database_to_list(_item_receive_index, item_array, max_items, false)
	database_to_list(_item_require_index, item_array, max_items, true)

func _reload_item_list() -> void:
	var selected_items : Array = _item_list.get_selected_items()
	var selected       : int   = -1 if selected_items.size() < 1 else selected_items[0]
	
	_item_list.clear()
	
	var item_array := Database.items
	for i in _cur_shop.inventory.size():
		var inv : ShopItem = _cur_shop.inventory[i]
		
		if inv.requires_item.index < 0:
			_item_list.add_item(
				str(i+1) + ": Gives %s for free" % _item_string(inv.receives_item)
			)
		else:
			_item_list.add_item(
				str(i+1) + ": Sells %s for %s" %
				[
					_item_string(inv.receives_item),
					_item_string(inv.requires_item)
				]
			)
	
	_item_list.deselect_all()
	if selected < 0: _on_item_list_item_selected(-1)
	else:            _item_list.select(selected)

func _item_string(item_slot: ItemSlot) -> String:
	return str(item_slot.amount) + "x " + Database.items[item_slot.index].name

#######################################
### Gui Functions
##############################

func _on_name_input_text_changed(new_text: String) -> void:
	_cur_shop.name = new_text
	notify_changes()

func _on_buy_rate_scroll_value_changed(value: float) -> void:
	_cur_shop.buy_rate = _buy_rate_scroll.value
	
	_buy_rate_label.text = "Buy Rate: " + str(value) + "%"
	notify_changes()

func _on_item_list_item_selected(index: int) -> void:
	# deselect handler
	if index < 0:
		_item_del_button.disabled = true
		
		_item_receive_index.select(-1)
		_item_receive_amount.value    = 1
		_item_receive_index.disabled  = true
		
		_item_require_index.select(-1)
		_item_require_amount.value    = 1
		_item_require_index.disabled  = true
		
		_item_update_button.disabled = true
	else:
		var inv : ShopItem = _cur_shop.inventory[index]
		
		_item_del_button.disabled = false
		
		_item_receive_index.select(inv.receives_item.index)
		_item_receive_amount.value    = inv.receives_item.amount
		_item_receive_index.disabled  = false
		
		_item_require_index.select(inv.requires_item.index + 1)
		_item_require_amount.value    = inv.requires_item.amount
		_item_require_index.disabled  = false
		
		_item_update_button.disabled = false

func _on_add_pressed() -> void:
	var new_item := ShopItem.new()
	new_item.receives_item.index  = 0
	new_item.receives_item.amount  = 1
	new_item.requires_item.index = -1
	new_item.requires_item.amount  = 1
	
	var selected_items : Array = _item_list.get_selected_items()
	var selected       : int   = -1 if selected_items.size() < 1 else selected_items[0]
	if selected < 0: _cur_shop.inventory.append(new_item)
	else: _cur_shop.inventory.insert(selected + 1, new_item)
	
	_reload_item_list()

func _on_delete_pressed() -> void:
	_cur_shop.inventory.remove_at(_item_list.get_selected_items()[0])
	_reload_item_list()

func _on_update_pressed() -> void:
	var selected : int   = _item_list.get_selected_items()[0]
	_cur_shop.inventory[selected].receives_item.index = _item_receive_index.selected
	_cur_shop.inventory[selected].receives_item.amount = int(_item_receive_amount.value)
	_cur_shop.inventory[selected].requires_item.index = _item_require_index.selected - 1
	_cur_shop.inventory[selected].requires_item.amount = int(_item_require_amount.value)
	_reload_item_list()

func _on_mouse_pressed() -> void: Audio.play_sound(Session.options.button_down)
func _on_mouse_entered() -> void: Audio.play_sound(Session.options.button_hover)
