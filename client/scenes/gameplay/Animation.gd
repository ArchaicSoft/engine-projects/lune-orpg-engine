class_name AnimationObj extends Sprite2D

@onready var animator : AnimationPlayer = $Animator

var expire_time : float = -1.0

func _physics_process(delta: float) -> void:
	if self.visible and expire_time < 0.0: return
	
	expire_time -= delta
	
	if expire_time < 0.0: return
	
	self.visible = false
	self.queue_free()
