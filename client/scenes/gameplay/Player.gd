class_name PlayerObj extends Node2D

@onready
var _name_tag        : Label                    = $NameTag
var _camera          : Camera2D
var _paperdolls      : Array[PaperdollObj]
var _movement_queue  : Array[MovementQueueInst]

# current character information cache's
var _char_access     : Globals.Access           = Globals.Access.player
var _char_looking_at : Globals.Direction        = Globals.Direction.down
var _char_move_state : MovementQueueInst        = MovementQueueInst.new()
var _char_sprite     : Array[int]               =\
	Utility.array_presize(Globals.Equipment.keys().size() + 1, -1)

func _ready() -> void:
	# load up paperdoll array
	_paperdolls.append_array([
		$Helmet,
		$Torso,
		$Shield,
		$Weapon,
		$Skin # always keep last
	])

func _on_tree_entered():
	# only perform this function if the player is ourselves
	if not Session.player_is_me(str(self.name).to_int()): return
	
	# load camera
	_camera = load(
		"res://scenes/gameplay/PlayerCamera.tscn"
	).instantiate(); self.add_child(_camera)

func _physics_process(delta: float) -> void:
	var id     : int    = str(self.name).to_int()
	var player : Player = Session.get_player(id)
	
	# ensure we still exist
	if not _self_exists(player):
		_self_destruct()
		return
	
	# process character info changes
	_process_sprite(player)
	_process_name_tag(player, id)
	
	# process translations
	_process_movement(player, delta)

func _self_exists(player: Player) -> bool:
	return not (
		player               == null or
		player.character     == null or 
		player.character.map != \
		Session.my_player().character.map
	)

func _self_destruct() -> void:
	self.set_process(false)
	self.set_physics_process(false)
	self.visible = false
	self.queue_free()

func _process_sprite(player: Player) -> void:
	# skin check
	var skin : int = player.character.sprite
	if _char_sprite[-1] != skin:
		var texture  : Texture2D = AssetLoader.load_player(skin)
		
		# change sprite
		_char_sprite[-1] = skin
		_paperdolls[-1].set_sprite(texture)
		
		# relocate sprites
		var frame_count : int = _paperdolls[-1].vframes
		var frame_size  : int = -texture.get_height() / frame_count
		var half_height : int = frame_size / 2
		for sprite in _paperdolls:
			sprite.position.y = half_height
		
		# relocate nametag
		_name_tag.position.y = frame_size - _name_tag.size.y - 12
	
	# equipment check
	for i in _char_sprite.size() - 1:
		var item_index : int = player.character.equipment[i].index
		var paperdoll  : int = -1
		if -1 < item_index and item_index < Globals.Max.items:
			paperdoll = Database.items[item_index].paperdoll
		
		# ensure different
		if _char_sprite[i] == paperdoll: continue
		
		# set equipment layer
		_char_sprite[i] = paperdoll
		_paperdolls[i].set_sprite(AssetLoader.load_paperdoll(paperdoll))

func _process_name_tag(player: Player, id: int) -> void:
	# update text
	if _name_tag.text != player.character.name:
		_name_tag.text = player.character.name
	
	if _char_access != player.account.access:
		_char_access = player.account.access
		_name_tag.add_theme_color_override("font_color",
			Color.from_string(Session.get_access_color(id), "white")
		)

func _process_movement(player: Player, delta: float) -> void:
	# update queue with new positions
	_check_movement(player)
	
	# make sure we have movement to process
	if _movement_queue.size() < 1: return
	
	# store absolute distance check
	var prev_movement : MovementQueueInst = _char_move_state
	var new_movement  : MovementQueueInst = _movement_queue[0]
	var tile_position : Vector2           = _tile_to_pos(new_movement.position)
	var distance      : Vector2           = Vector2(
		abs(new_movement.position.x - prev_movement.position.x),
		abs(new_movement.position.y - prev_movement.position.y)
	)
	
	# make sure speed isnt 0
	if new_movement.move_speed <= 0.0:
		new_movement.move_speed = 1.0
	
	# teleport if distance is larger than 1 tile or no speed
	if distance.x > 1 or distance.y > 1:
		self.position = tile_position
	
	# lerp the distance
	else:
		var speed : float = Globals.TILE_SIZE / new_movement.move_speed * delta
		self.position += (new_movement.position - prev_movement.position) * speed
	
	# determine if need to re-orient the layers
	if _char_looking_at != new_movement.move_dir:
		_char_looking_at = new_movement.move_dir
		
		# update paperdoll positinos
		self.move_child(_paperdolls[-1], 0) # skin
		match new_movement.move_dir:
			Globals.Direction.up:
				self.move_child(_paperdolls[Globals.Equipment.weapon], 1)
				self.move_child(_paperdolls[Globals.Equipment.shield], 2)
				self.move_child(_paperdolls[Globals.Equipment.helmet], 3)
				self.move_child(_paperdolls[Globals.Equipment.torso],  4)
			
			Globals.Direction.left:
				self.move_child(_paperdolls[Globals.Equipment.helmet], 1)
				self.move_child(_paperdolls[Globals.Equipment.torso],  2)
				self.move_child(_paperdolls[Globals.Equipment.shield], 3)
				self.move_child(_paperdolls[Globals.Equipment.weapon], 4)
			
			Globals.Direction.right:
				self.move_child(_paperdolls[Globals.Equipment.helmet], 1)
				self.move_child(_paperdolls[Globals.Equipment.torso],  2)
				self.move_child(_paperdolls[Globals.Equipment.shield], 3)
				self.move_child(_paperdolls[Globals.Equipment.weapon], 4)
			
			Globals.Direction.down:
				self.move_child(_paperdolls[Globals.Equipment.helmet], 1)
				self.move_child(_paperdolls[Globals.Equipment.torso],  2)
				self.move_child(_paperdolls[Globals.Equipment.shield], 3)
				self.move_child(_paperdolls[Globals.Equipment.weapon], 4)
	
	# play animation
	var anim_speed : float = 1 # new_movement.move_speed
	var anim_name  : String = "%s_%s" % [
		Globals.Movement.keys()[new_movement.move_type],
		Globals.Direction.keys()[new_movement.move_dir]
	]
	
	for i in _char_sprite.size():
		_paperdolls[i].play_animation(anim_name, anim_speed)
	
	# determine if we are in place to remove queue
	if (
		abs(self.position.x - tile_position.x) < 0.5 and
		abs(self.position.y - tile_position.y) < 0.5
	):
		self.position    = tile_position
		_char_move_state = new_movement
		_movement_queue.remove_at(0)
		
		# make sure we become standing if no final movements
		if (
			_movement_queue.size() < 1 and
			new_movement.move_type != Globals.Movement.standing
		):
			player.instance.move_type = Globals.Movement.standing

func _tile_to_pos(coords: Vector2) -> Vector2:
	var size   : int = Globals.TILE_SIZE
	var offset : int = int(size / 2)
	return Vector2(
		int(coords.x * size + offset),
		int(coords.y * size + offset)
	)

func _check_movement(player: Player) -> void:
	var check_queue : MovementQueueInst
	
	# determine if we are using current location or top of the stack
	if _movement_queue.size() < 1: check_queue = _char_move_state
	else: check_queue = _movement_queue[-1]
	
	# detect movement since last queue status
	if (
		player.character.direction == check_queue.move_dir  and
		player.instance.move_type  == check_queue.move_type  and
		player.instance.move_speed == check_queue.move_speed and
		player.character.pos_x     == check_queue.position.x and
		player.character.pos_y     == check_queue.position.y
	): return
	
	# append movement update to queue
	_movement_queue.append(
		MovementQueueInst.new(
			player.character.direction,
			player.instance.move_type,
			player.instance.move_speed,
			player.character.get_position()
		)
	) 


