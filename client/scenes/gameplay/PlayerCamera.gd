class_name PlayerCamera
extends Camera2D

func _physics_process(_delta: float):
	var map        : Map = Session.get_current_map()
	
	# get view properties
	var view_size  : Vector2 = GameView.instance().viewport.size
	var tile_size  : float   = Globals.TILE_SIZE
	
	# get lower map bound (or 16 tiles max)
	var min_bound  : float = mini(16, mini(map.size.x, map.size.y)) * tile_size
	
	# get larger screen bound
	var max_bound  : float = max(view_size.x, view_size.y)
	
	# assign camera zoom
	var view_scale : float = 1.0 / (min_bound / max_bound)
	self.zoom = Vector2(view_scale, view_scale)
	
	# assign camera limits
	self.limit_right  = tile_size * map.size.x
	self.limit_bottom = tile_size * map.size.y
