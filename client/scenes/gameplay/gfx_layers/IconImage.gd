class_name IconImageObj extends Sprite2D

@onready var animator : AnimationPlayer = $Animator

func set_image_item(index: int) -> void:
	# clear texture
	if index < 0:
		self.texture = null
		return
	
	# load texture
	self.texture = AssetLoader.load_item(index)
