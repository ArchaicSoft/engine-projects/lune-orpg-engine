class_name PaperdollObj extends Sprite2D

@onready var animator : AnimationPlayer = $Animator

func set_sprite(image: Texture2D) -> void:
	self.texture = image

func play_animation(animation: String, speed: float = 1.0) -> void:
	# set new animation
	if animator.current_animation != animation:
		animator.play(animation)
	
	# change time
	if animator.speed_scale != speed:
		animator.speed_scale = speed
