class_name MapObj extends TileMap

var invalid_tileset_index : int = -1

func _ready() -> void:
	# add tileset manager
	self.tile_set = TileSet.new()
	self.tile_set.tile_size = Vector2i(Globals.TILE_SIZE, Globals.TILE_SIZE)
	
	# add map layers
	var layers := Globals.MapLayer.keys()
	for i in layers.size():
		self.add_layer(i)
		self.set_layer_name(i, layers[i])
	
	# load tileset data
	var tile_size   : Vector2i = Vector2i(Globals.TILE_SIZE, Globals.TILE_SIZE)
	var cur_texture : int      = 0
	
	# loop-load textures until no more are found
	while invalid_tileset_index < 0:
		# try to load texture
		var texture : Texture2D = AssetLoader.load_tileset(cur_texture, false)
		if texture == null:
			# load 'missing-tileset' placeholder
			texture = AssetLoader.load_system("missing/tile")
			invalid_tileset_index = cur_texture
		
		# create atlas for texture
		var atlas : TileSetAtlasSource = TileSetAtlasSource.new()
		atlas.texture_region_size      = tile_size
		atlas.texture                  = texture
		
		# create tile list
		var texture_size : Vector2i = texture.get_size() / tile_size.x
		for y in texture_size.y: for x in texture_size.x:
			atlas.create_tile(Vector2i(x, y))
		
		# add the atlas (texture) to tilemap
		self.tile_set.add_source(atlas, cur_texture)
		cur_texture += 1
	
	# draw current map
	draw_map(Session.get_current_map())

func draw_map(map: Map) -> void:
	# clear existing render data
	self.clear()
	
	var map_size  : Vector2i = map.size
	var dst_pos   : Vector2i = Vector2i.ZERO
	var src_pos   : Vector2i = Vector2i.ZERO
	const UNSET   : int      = 0xFFFF
	
	for layer in Globals.MapLayer.size():
		var layer_type :         = layer as Globals.MapLayer
		
		for y in map_size.y: for x in map_size.x:
			var tile       : Tile = map.get_tile(x, y)
			var tileset    : int  = tile.get_layer_tileset(layer_type)
			
			# continue if no tileset is loaded
			if tileset == UNSET: continue
			
			# get map and tileset coordinates
			dst_pos = Vector2i(x, y)
			src_pos = tile.get_layer_xy(layer_type)
			
			# draw if valid tileset and coordinates
			if (
				self.tile_set.has_source(tileset) and
				self.tile_set.get_source(tileset).has_tile(src_pos)
			): self.set_cell(layer, dst_pos, tileset, src_pos)
			
			# render 'missing asset' tile
			else:
				self.set_cell(layer, dst_pos, invalid_tileset_index, Vector2.ZERO)
