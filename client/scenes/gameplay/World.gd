class_name WorldObj extends Node2D
static func instance() -> WorldObj:
	var parent = GameView.instance()
	return null if parent == null else parent.world as WorldObj

@onready var map     : MapObj   = $Map
@onready var npcs    : Node2D   = $Npcs
@onready var players : Node2D   = $Players
