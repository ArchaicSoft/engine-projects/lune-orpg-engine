class_name GlobalOverlay extends ColorRect

@onready var _overlay_scn_msgbox : PackedScene = load("res://scenes/global/overlay/MessageBox.tscn")
@onready var _overlay_scn_status : PackedScene = load("res://scenes/global/overlay/Status.tscn")

var _overlay_queue : Array[OverlayObject]
var _gui_focus     : Control

func _ready() -> void:
	# format our layouts
	_detect_focus()

func _add_to_queue(obj: OverlayObject) -> void:
	obj.visible = false
	self.add_child(obj)
	obj.owner = self
	
	_overlay_queue.append(obj)
	_detect_focus()

func _remove_from_queue(obj: OverlayObject) -> void:
	for ovl in _overlay_queue.size() - 1:
		if _overlay_queue[ovl] == obj:
			_overlay_queue.remove_at(ovl)
	
	obj.overlay_focused = false
	obj.visible = false
	obj.queue_free()
	_detect_focus()

func _detect_focus() -> void:
	# show and focus our queue'd object if one exists
	if _overlay_queue.size() > 0:
		# make sure obj is not dead
		if (
			_overlay_queue[0] == null or 
			_overlay_queue[0].is_queued_for_deletion()
		):
			_overlay_queue.remove_at(0)
			_detect_focus()
			return
		
		_overlay_queue[0].overlay_focused = true
		_overlay_queue[0].visible = true
		
		# cache focus point info
		var control : Control = get_tree().root.gui_get_focus_owner()
		if not visible and is_instance_valid(control):
			_gui_focus = control
		get_tree().root.gui_release_focus()
		
		# move us to front of scene
		var root = get_tree().root
		root.call_deferred("move_child", self, root.get_child_count())
		self.visible = true
	
	# hide the unused overlay
	else:
		self.visible = false
		
		# reclaim old focus point
		if is_instance_valid(_gui_focus):
			_gui_focus.grab_focus()

#######################################
### MessageBox Functions
##############################

func message_box(
	message  : String,
	title    : String             = "",
	buttons  : MessageBox.Buttons = MessageBox.Buttons.okay,
	icon     : MessageBox.Icons   = MessageBox.Icons.no_icon,
	callback : Callable           = Callable(func(r: MessageBox.Response): pass)
) -> void:
	var obj := _overlay_scn_msgbox.instantiate()
	
	# get controls for setup
	var title_bar  : Label         = obj.get_node("Align/TitleBar/Bounds/Text")
	var text_body  : RichTextLabel = obj.get_node("Align/Contents/Scroll/Text")
	var button_bar : Control       = obj.get_node("Align/Buttons/Align")
	
	# set title bar
	title_bar.text = title
	
	# set text contents
	text_body.text = message
	
	# add icon to text contents
#	if icon != Icons.no_icon:
#		var path = "res://assets/images/gui/overlay/%s.png" % Icons.keys()[icon]
#		text_body.add_image(load(path) as Texture2D)
	
	# setup the button visibility and signals
	for btn in button_bar.get_children():
		btn.visible = false
	
	match buttons:
		MessageBox.Buttons.okay:
			button_bar.get_node("Okay").visible = true
		
		MessageBox.Buttons.yes_no:
			button_bar.get_node("YesNo").visible = true
		
		MessageBox.Buttons.accept_cancel:
			button_bar.get_node("AcceptCancel").visible = true
	
	obj.callback = callback
	
	# queue up
	_add_to_queue(obj)

#######################################
### Status Functions
##############################

func set_status(
	message      : String,
	auto_center  : bool = true,
	animate_tick : bool = true,
	timeout      : float = 7.0,
	callback     : Callable = Callable()
) -> void:
	var obj := _overlay_scn_status.instantiate()
	
	# status text
	obj.status_text = ("[center]" if auto_center else "") + message
	
	# period animation
	obj.tick_active = animate_tick
	
	# timeout
	if timeout > 0.0:
		obj.timeout_dur = timeout
		obj.callback    = callback
	
	# queue up
	_add_to_queue(obj)

func clear_status(need_focus: bool = true) -> void:
	# make sure we are put up front even with no status
	if need_focus: _detect_focus()
	
	for ovl in range(_overlay_queue.size() - 1, -1, -1):
		if _overlay_queue[ovl] == null: continue
		if _overlay_queue[ovl].type_info() == "StatusLabel":
			_remove_from_queue(_overlay_queue[ovl])
