class_name GlobalAudio extends Node

@onready var music_player : AudioStreamPlayer = $Music
@onready var sound_player : AudioStreamPlayer = $Sound

var current_music : String

func play_music(file: String, restart_identical: bool = false, ignore_error: bool = false) -> void:
	# only process if can be valid audio file
	if file.strip_escapes().length() < 4: return
	
	# just keep playing the song requested
	if current_music == file:
		if restart_identical:
			music_player.seek(0.0)
		return
	
	# attempt loading file
	var audio_file : Variant = AssetLoader.load_music(file, ignore_error)
	if audio_file == null:
		music_player.stop()
		return
	
	# load new file and play
	music_player.stream = audio_file
	music_player.play()
	
	# track what we just set to
	current_music = file

func play_sound(file: String, ignore_error: bool = false) -> void:
	# only process if can be valid audio file
	if file.strip_escapes().length() < 4: return
	
	# attempt loading file
	var audio_file : Variant = AssetLoader.load_sound(file, ignore_error)
	if audio_file == null: return
	
	# load new file and play
	sound_player.stream = audio_file
	sound_player.play()

func set_music_volume(volume: float) -> void:
	music_player.volume_db = -80 + int((clampf(volume, 0.0, 1.0) * 80))

func set_sound_volume(volume: float) -> void:
	sound_player.volume_db = -80 + int((clampf(volume, 0.0, 1.0) * 80))
