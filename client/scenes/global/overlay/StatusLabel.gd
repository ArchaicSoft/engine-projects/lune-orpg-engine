class_name StatusLabel
extends OverlayObject
func type_info() -> String: return "StatusLabel"

# status message info
var status_text    : String

# period animation
var tick_count     : float
var tick_active    : bool

# timeout to kill the object
var timeout_cur    : float
var timeout_dur    : float

func _physics_process(delta: float) -> void:
	if not overlay_focused: return
	
	# animate the dots on the end of status
	if tick_active:
		tick_count += (delta * 2) # produce change every half second
		if tick_count >= 4.0: tick_count -= 4.0
	
	self.text = status_text + ".".repeat(int(tick_count))
	
	# timeout callback
	if timeout_dur > 0.0:
		timeout_cur += delta
		if timeout_cur >= timeout_dur:
			Overlay._remove_from_queue(self)
			if self.callback.is_valid():
				self.callback.call()
