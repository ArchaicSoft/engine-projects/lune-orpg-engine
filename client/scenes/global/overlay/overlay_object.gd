class_name OverlayObject
extends Control
func type_info() -> String: return "OverlayObject"

var callback        : Callable
var overlay_focused : bool
