class_name MessageBox
extends OverlayObject
func type_info() -> String: return "MessageBox"

enum Buttons {
	okay,
	yes_no,
	accept_cancel,
}

enum Icons {
	no_icon,
	warning,
	error
}

enum Response {
	positive,
	negative
}

func _respond_positive() -> void: _respond(Response.positive)
func _respond_negative() -> void: _respond(Response.negative)

func _respond(response: Response) -> void:
	Overlay._remove_from_queue(self)
	if self.callback.is_valid():
		self.callback.call(response)

func _on_mouse_pressed() -> void: Audio.play_sound(Session.options.button_down)
func _on_mouse_entered() -> void: Audio.play_sound(Session.options.button_hover)
